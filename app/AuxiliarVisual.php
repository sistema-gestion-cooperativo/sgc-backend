<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuxiliarVisual extends Model
{
    protected $table = 'auxiliares_visuales';

    protected $fillable = [
      'concepto',
      'objeto'
    ];
}
