<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Cotizacion extends Model
{
    use Searchable;

    protected $table = 'cotizaciones';

    protected $fillable = [
        'centro_costo_id',
        'cliente_id',
        'cliente',
        'codigo',
        'codigo_modificacion',
        'comentarios',
        'cotizacion_id',
        'unidad_productiva_id',
        'estado_cotizacion_id',
        'fecha_envio',
        'fecha_solicitud',
        'monto_afecto',
        'monto_exento',
        'monto_descuento',
        'monto_iva',
        'monto_neto',
        'monto_total',
        'monto_utilidad',
        'nombre_presentacion',
        'porcentaje_descuento',
        'porcentaje_utilidad',
        'responsable_cotizacion_id',
        'responsable_proyecto_id',
        'tipo_cotizacion_id',
        'tipo_documento_id',
        'validez',
        'valor_cip',
        'valor_hora',
        'valor_uf',
    ];

    /**
     * Precarga las relaciones indicadas.
     *
     * @var array
     */
    protected $with = [
        'tipo',
        'estado'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public static function nuevoCodigo()
    {
        $ultimoCodigo = Cotizacion::max('codigo');
        $proximoCodigo = ++$ultimoCodigo;
        return ["codigo" => $proximoCodigo];
    }

    // Relations
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function responsable()
    {
        return $this->belongsTo('App\Persona', 'responsable_id');
    }

    public function tipo()
    {
        return $this->belongsTo('App\TipoCotizacion', 'tipo_cotizacion_id');
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\TipoDocumento', 'tipo_documento_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoCotizacion', 'estado_cotizacion_id');
    }

    public function partidas()
    {
        return $this->hasMany('App\Partida');
    }
    public function cotizacionAnterior()
    {
        return $this->belongsTo('App\Cotizacion', 'cotizacion_id');
    }



    /**
     * Métodos
     */
    public function presupuestoMateriales()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_materiales;
        }, 0);
    }

    public function presupuestoManoDeObra()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_mano_de_obra;
        }, 0);
    }

    public function presupuestoExcedentes()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_excedentes;
        }, 0);
    }

    public function presupuestoHospedaje()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_hospedaje;
        }, 0);
    }

    public function presupuestoViatico()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_viatico;
        }, 0);
    }

    public function presupuestoTraslado()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_traslado;
        }, 0);
    }

    public function presupuestoOtros()
    {
        return $this->partidas()->reduce(function ($carry, $item) {
            return $carry + $item->presupuesto_otros;
        }, 0);
    }
    /**
     * Scopes
     */


    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'cotizaciones';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        if (!is_null($this->cliente)) {
            $array['cliente_rut'] = $this->cliente->rut;
            $array['cliente_nombre'] = $this->cliente->nombre;
            $array['cliente_nombre_presentacion'] = $this->cliente->nombre_presentacion;
        }
        if (!is_null($this->responsable)) {
            $array['responsable'] = $this->responsable->nombre_presentacion;
        }
        if (!is_null($this->estado)) {
            $array['estado'] = $this->estado->nombre_presentacion;
        }
        if (!is_null($this->tipo)) {
            $array['tipo'] = $this->tipo->nombre_presentacion;
        }

        return $array;
    }
}
