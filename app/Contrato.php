<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $fillable = [
      'fecha_firma',
      'fecha_vigencia',
      'fecha_vencimiento'
    ];
}
