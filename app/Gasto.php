<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Gasto extends Model
{
    use Searchable;

    protected $fillable = [
        'periodo_mes',
        'periodo_ano',
        'centro_costo_id',
        'proveedor_id',
        'tipo_documento_id',
        'tipo_gasto_id',
        'periodo',
        'fecha',
        'folio',
        'monto_exento',
        'monto_afecto',
        'monto_otros_impuestos',
        'comentarios',
    ];

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'gastos';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }


  // Relations
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto');
    }
    public function tipoGasto()
    {
        return $this->belongsTo('App\TipoGasto', 'tipos_gasto_id');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor', 'proveedor_id');
    }
    public function cuenta()
    {
        return $this->belongsTo('App\Cuenta', 'cuenta_id');
    }

//     proveedor_id
// tipo_documento_id
// tipo_documento_pago_id
// centro_costo_id
// cuenta_id

  //Mutations
    // public function setMontoNetoAttribute($value)
    // {
    //     $this->attributes['monto_neto'] = $this->attributes['monto_afecto'] + $this->attributes['monto_exento'];
    // }
    // public function setMontoIvaAttribute($value)
    // {
    //     $this->attributes['monto_iva'] = $this->attributes['monto_afecto'] + $this->attributes['monto_afecto'] * 0.19;
    // }
    // public function setMontoTotalAttribute($value)
    // {
    //     $this->attributes['monto_total'] = $value;
    // }
}
