<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
      'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        Gate::resource('periodo', 'PeriodoPolicy');
        Gate::resource('unidad-productiva', 'UnidadProductivaPolicy');
        Gate::resource('rol', 'RolPolicy');
        Gate::resource('centro-costo', 'CentroCostoPolicy');
        Gate::resource('cotizacion', 'CotizacionPolicy');
        Gate::resource('cliente', 'ClientePolicy');
        Gate::resource('proveedor', 'ProveedorPolicy');
        Gate::resource('gasto', 'GastoPolicy');
        Gate::resource('persona', 'PersonaPolicy');
        Gate::resource('cuenta', 'CuentaPolicy');
        Gate::resource('cuenta-bancaria', 'CuentaBancariaPolicy');
    }
}
