<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class TipoCotizacion extends Model
{
    protected $table = 'tipos_cotizacion';

    protected $fillable = [
        'codigo',
        'nombre_presentacion'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
}
