<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    protected $fillable = [
      'mes',
      'ano',
      'nombre_presentacion',
      'fecha_inicio',
      'fecha_termino',
      'abierto'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
}
