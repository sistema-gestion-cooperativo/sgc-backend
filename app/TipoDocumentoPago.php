<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumentoPago extends Model
{
    protected $table = 'tipos_documento_pago';

    protected $fillable = [
      'nombre_presentacion'
    ];
}
