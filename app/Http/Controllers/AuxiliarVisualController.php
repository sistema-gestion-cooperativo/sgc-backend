<?php

namespace App\Http\Controllers;

use App\AuxiliarVisual;
use App\Http\Resources\AuxiliarVisualResource;
use Illuminate\Http\Request;

class AuxiliarVisualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return new AuxiliarVisualResource(AuxiliarVisual::all());
    }

}
