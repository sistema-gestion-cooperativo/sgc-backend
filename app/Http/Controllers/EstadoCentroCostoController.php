<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoCentroCostoResource;
use App\EstadoCentroCosto;
use Illuminate\Http\Request;

class EstadoCentroCostoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoCentroCostoResource::collection(EstadoCentroCosto::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoCentroCosto::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoCentroCosto $EstadoCentroCosto
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoCentroCosto $EstadoCentroCosto
     */
    public function show(EstadoCentroCosto $EstadoCentroCosto)
    {
        return new EstadoCentroCostoResource($EstadoCentroCosto);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoCentroCosto             $EstadoCentroCosto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCentroCosto $EstadoCentroCosto)
    {
        $input = $request->all();
        $result = $EstadoCentroCosto->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCentroCosto $EstadoCentroCosto
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCentroCosto $EstadoCentroCosto)
    {
        $result = $EstadoCentroCosto->delete();

        return response(['deleted' => $result], 204);
    }
}
