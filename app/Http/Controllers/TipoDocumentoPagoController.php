<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoDocumentoPagoResource;
use App\TipoDocumentoPago;
use Illuminate\Http\Request;

class TipoDocumentoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TipoDocumentoPagoResource(TipoDocumentoPago::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoDocumentoPago::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoDocumentoPago $tipo_documento_pago
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoDocumentoPago $tipo_documento_pago
     */
    public function show(TipoDocumentoPago $tipo_documento_pago)
    {
        TipoDocumentoPagoResource::withoutWrapping();

        return new TipoDocumentoPagoResource($tipo_documento_pago);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoDocumentoPago $tipo_documento_pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoDocumentoPago $tipo_documento_pago)
    {
        $input = $request->all();
        $result = $tipo_documento_pago->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoDocumentoPago $tipo_documento_pago
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoDocumentoPago $tipo_documento_pago)
    {
        $result = $tipo_documento_pago->delete();

        return response(['deleted' => $result], 204);
    }
}
