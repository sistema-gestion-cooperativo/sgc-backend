<?php

namespace App\Http\Controllers;

use App\CajaCompensacion;
use App\Http\Resources\CajaCompensacionResource;
use Illuminate\Http\Request;

class CajaCompensacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CajaCompensacionResource(CajaCompensacion::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = CajaCompensacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\CajaCompensacion $caja_compensacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\CajaCompensacion $caja_compensacion
     */
    public function show(CajaCompensacion $caja_compensacion)
    {
        CajaCompensacionResource::withoutWrapping();

        return new CajaCompensacionResource($caja_compensacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CajaCompensacion $caja_compensacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CajaCompensacion $caja_compensacion)
    {
        $input = $request->all();
        $result = $caja_compensacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CajaCompensacion $caja_compensacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(CajaCompensacion $caja_compensacion)
    {
        $result = $caja_compensacion->delete();

        return response(['deleted' => $result], 204);
    }
}
