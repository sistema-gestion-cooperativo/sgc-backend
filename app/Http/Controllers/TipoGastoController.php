<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoGastoResource;
use App\TipoGasto;
use Illuminate\Http\Request;

class TipoGastoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoGastoResource::collection(TipoGasto::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoGasto::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoGasto $tipo_gasto
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoGasto $tipo_gasto
     */
    public function show(TipoGasto $tipo_gasto)
    {
        return new TipoGastoResource($tipo_gasto);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoGasto             $tipo_gasto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoGasto $tipo_gasto)
    {
        $input = $request->all();
        $result = $tipo_gasto->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoGasto $tipo_gasto
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoGasto $tipo_gasto)
    {
        $result = $tipo_gasto->delete();

        return response(['deleted' => $result], 204);
    }
}
