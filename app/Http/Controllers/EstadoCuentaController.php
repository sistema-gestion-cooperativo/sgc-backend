<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoCuentaResource;
use App\EstadoCuenta;
use Illuminate\Http\Request;

class EstadoCuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoCuentaResource::collection(EstadoCuenta::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoCuenta::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoCuenta $estadoCuenta
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoCuenta $estadoCuenta
     */
    public function show(EstadoCuenta $estadoCuenta)
    {
        return new EstadoCuentaResource($estadoCuenta);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoCuenta             $estadoCuenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCuenta $estadoCuenta)
    {
        $input = $request->all();
        $result = $estadoCuenta->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCuenta $estadoCuenta
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCuenta $estadoCuenta)
    {
        $result = $estadoCuenta->delete();

        return response(['deleted' => $result], 204);
    }
}
