<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChequeResource;
use App\Cheque;
use Illuminate\Http\Request;

class ChequeController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Cheque;
        

        if ($request->has('nuevo-codigo')) {
            return new ChequeResource($model->where('codigo', $model::max('codigo'))->first());
        }

        return ChequeResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Cheque::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Cheque $cheque
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Cheque $cheque
     */
    public function show(Cheque $cheque)
    {
        return new ChequeResource($cheque);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Cheque             $cheque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cheque $cheque)
    {
        $input = $request->all();
        $result = $cheque->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cheque $cheque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cheque $cheque)
    {
        $result = $cheque->delete();

        return response(['deleted' => $result], 204);
    }
}
