<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoActaResource;
use App\TipoActa;
use Illuminate\Http\Request;

class TipoActaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoActaResource::collection(TipoActa::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoActa::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoActa $tipo_acta
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoActa $tipo_acta
     */
    public function show(TipoActa $tipo_acta)
    {
        return new TipoActaResource($tipo_acta);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoActa             $tipo_acta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoActa $tipo_acta)
    {
        $input = $request->all();
        $result = $tipo_acta->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoActa $tipo_acta
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoActa $tipo_acta)
    {
        $result = $tipo_acta->delete();

        return response(['deleted' => $result], 204);
    }
}
