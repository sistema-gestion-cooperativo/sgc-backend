<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class ListableController extends Controller
{
    protected function coleccion(Request $request, Model $model, $where = null)
    {
        $dynSort = false;
        $rowsPerPage = $request->query('rowsPerPage', 10);
        $sortBy = $request->query('sortBy', 'id');
        $order = filter_var($request->query('descending', false), FILTER_VALIDATE_BOOLEAN);
        $query = $request->query('query');
        $page = $request->query('page', 1);

        /**
         * En caso de existir el campo Fields, se devuelve una colección con todos los elementos, pero sólo con los
         * atributos seleccionados, por defecto 'id' y 'nombre_presentacion'.
         * Esto sirve para llenar listados seleccionables o revisar rápidamente una tabla.
         */
        if ($request->has('fields')) {
            $fields = $request->query('fields');
            $availableFields = array_merge(['id', 'nombre_presentacion'], $model->getFillable());
            abort_if(count(array_diff($fields, $availableFields)), 403, 'Se solicitan valores no permitidos o inexistentes');
            if ($request->has('full')) {
                //TODO: Habrá forma más decente de eliminar los scopes en todo?
                return $model::withoutGlobalScope(VisibleScope::class)->select($fields)->get();
            }
            return $model::select($fields)->get();
        }

        if (!is_null($sortBy) &&
            !in_array($sortBy, Schema::getColumnListing($model->getTable())) &&
            in_array($sortBy . '_id', Schema::getColumnListing($model->getTable()))) {
            $sortBy .= '_id';
        } elseif (!is_null($model->$sortBy)) {
            //Si es una propiedad dinámica que tiene valor diferente a null en $attributes
            $dynSort = true;
        }

        /**
         * En caso de pasar el parámetro where, se ejecuta una búsqueda en la base de datos
         */
        if (!is_null($where)) {
            if ($request->has('full')) {
                //TODO: Habrá forma más decente de eliminar los scopes en todo?
                return $model::withoutGlobalScope(VisibleScope::class)->where($where)
                ->orderBy($sortBy, $order ? 'desc' : 'asc')
                ->paginate($rowsPerPage);
            }
            return $model::where($where)
                ->orderBy($sortBy, $order ? 'desc' : 'asc')
                ->paginate($rowsPerPage);
        }

        //TODO: Revisar el objeto de colecction devuelto por search->get() y all(), para comprobar si es posible reducir un if
        // tentativo: return $model::search($query)->get()->sortBy($sortBy, SORT_REGULAR, $order)->paginate($rowsPerPage); y si $query es null, da igual ya que debería devolver toda la colección
        if (!is_null($query)) {
            if ($request->has('full')) {
                //TODO: Habrá forma más decente de eliminar los scopes en todo?
                return $model::withoutGlobalScope(VisibleScope::class)->search($query)
                ->get()
                ->sortBy($sortBy, SORT_REGULAR, $order)
                ->paginate($rowsPerPage);
            }
            return $model::search($query)
                ->get()
                ->sortBy($sortBy, SORT_REGULAR, $order)
                ->paginate($rowsPerPage);
        } elseif ($dynSort) {
            if ($request->has('full')) {
                //TODO: Habrá forma más decente de eliminar los scopes en todo?
                return $model::withoutGlobalScope(VisibleScope::class)->all()
                ->sortBy($sortBy, SORT_REGULAR, $order)
                ->paginate($rowsPerPage);
            }
            return $model::all()
                ->sortBy($sortBy, SORT_REGULAR, $order)
                ->paginate($rowsPerPage);
        } else {
            if ($request->has('full')) {
                //TODO: Habrá forma más decente de eliminar los scopes en todo?
                return $model::withoutGlobalScope(VisibleScope::class)->orderBy($sortBy, $order ? 'desc' : 'asc')
                ->paginate($rowsPerPage);
            }
            return $model::orderBy($sortBy, $order ? 'desc' : 'asc')
                ->paginate($rowsPerPage);
        }
    }
}
