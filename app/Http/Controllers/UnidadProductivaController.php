<?php

namespace App\Http\Controllers;

use App\UnidadProductiva;
use App\Http\Resources\UnidadProductivaResource;
use Illuminate\Http\Request;

class UnidadProductivaController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new UnidadProductiva;

        return UnidadProductivaResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = UnidadProductiva::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\UnidadProductiva $unidad_productiva
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\UnidadProductiva $unidad_productiva
     */
    public function show(UnidadProductiva $unidad_productiva)
    {
        UnidadProductiva::withoutWrapping();

        return new UnidadProductiva($unidad_productiva);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\UnidadProductiva $unidad_productiva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnidadProductiva $unidad_productiva)
    {
        $input = $request->all();
        $result = $unidad_productiva->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnidadProductiva $unidad_productiva
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnidadProductiva $unidad_productiva)
    {
        $result = $unidad_productiva->delete();

        return response(['deleted' => $result], 204);
    }
}
