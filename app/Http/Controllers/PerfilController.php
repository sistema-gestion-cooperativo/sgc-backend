<?php

namespace App\Http\Controllers;

use App\Http\Resources\PerfilResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    public function index(Request $request)
    {
        return new PerfilResource($request);
    }
}
