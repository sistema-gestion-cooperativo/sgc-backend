<?php

namespace App\Http\Controllers;

use App\Http\Resources\PaisResource;
use App\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return new PaisResource(Pais::all());
    }
}
