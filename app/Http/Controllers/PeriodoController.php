<?php

namespace App\Http\Controllers;

use App\Http\Resources\PeriodoResource;
use App\Periodo;
use Illuminate\Http\Request;

class PeriodoController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Periodo;
            

        if ($request->has('nuevo-codigo')) {
            return new PeriodoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return PeriodoResource::collection(parent::coleccion($request, $model));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Periodo::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Periodo $periodo
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Periodo $periodo
     */
    public function show(Periodo $periodo)
    {
        PeriodoResource::withoutWrapping();

        return new PeriodoResource($periodo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Periodo $periodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Periodo $periodo)
    {
        $input = $request->all();
        $result = $periodo->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Periodo $periodo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periodo $periodo)
    {
        $result = $periodo->delete();

        return response(['deleted' => $result], 204);
    }
}
