<?php

namespace App\Http\Controllers;

use App\Gasto;
use App\Http\Resources\GastoResource;
use Illuminate\Http\Request;

class GastoController extends ListableController
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Gasto;
            
        if ($request->has('nuevo-codigo')) {
            return new GastoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return GastoResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        
        $result = Gasto::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Gasto $gasto
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Gasto $gasto
     */
    public function show(Gasto $gasto)
    {
        GastoResource::withoutWrapping();

        return new GastoResource($gasto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Gasto $gasto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gasto $gasto)
    {
        $input = $request->all();
        $result = $gasto->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gasto $gasto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gasto $gasto)
    {
        $result = $gasto->delete();

        return response(['deleted' => $result], 204);
    }
}
