<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoCuentaBancariaResource;
use App\EstadoCuentaBancaria;
use Illuminate\Http\Request;

class EstadoCuentaBancariaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoCuentaBancariaResource::collection(EstadoCuentaBancaria::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoCuentaBancaria::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoCuentaBancaria $estadoCuentaBancaria
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoCuentaBancaria $estadoCuentaBancaria
     */
    public function show(EstadoCuentaBancaria $estadoCuentaBancaria)
    {
        return new EstadoCuentaBancariaResource($estadoCuentaBancaria);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoCuentaBancaria             $estadoCuentaBancaria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCuentaBancaria $estadoCuentaBancaria)
    {
        $input = $request->all();
        $result = $estadoCuentaBancaria->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCuentaBancaria $estadoCuentaBancaria
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCuentaBancaria $estadoCuentaBancaria)
    {
        $result = $estadoCuentaBancaria->delete();

        return response(['deleted' => $result], 204);
    }
}
