<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoCotizacionResource;
use App\EstadoCotizacion;
use Illuminate\Http\Request;

class EstadoCotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new EstadoCotizacionResource(EstadoCotizacion::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoCotizacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoCotizacion $estadocotizacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoCotizacion $estadocotizacion
     */
    public function show(EstadoCotizacion $estadocotizacion)
    {
        EstadoCotizacionResource::withoutWrapping();
        return new EstadoCotizacionResource($estadocotizacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoCotizacion $estadocotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCotizacion $estadocotizacion)
    {
        $input = $request->all();
        $result = $estadocotizacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCotizacion $estadocotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCotizacion $estadocotizacion)
    {
        $result = $estadocotizacion->delete();

        return response(['deleted' => $result], 204);
    }
}
