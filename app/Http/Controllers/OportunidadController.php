<?php

namespace App\Http\Controllers;

use App\Http\Resources\OportunidadResource;
use App\Oportunidad;
use Illuminate\Http\Request;

class OportunidadController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Oportunidad;
            

        if ($request->has('nuevo-codigo')) {
            return new OportunidadResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return OportunidadResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Oportunidad::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Oportunidad $oportunidad
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Oportunidad $oportunidad
     */
    public function show(Oportunidad $oportunidad)
    {
        OportunidadResource::withoutWrapping();

        return new OportunidadResource($oportunidad);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Oportunidad $oportunidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oportunidad $oportunidad)
    {
        $input = $request->all();
        $result = $oportunidad->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oportunidad $oportunidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oportunidad $oportunidad)
    {
        $result = $oportunidad->delete();

        return response(['deleted' => $result], 204);
    }
}
