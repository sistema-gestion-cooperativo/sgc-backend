<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoClienteResource;
use App\TipoCliente;
use Illuminate\Http\Request;

class TipoClienteController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new TipoCliente;
        

        if ($request->has('nuevo-codigo')) {
            return new TipoClienteResource($model->where('codigo', $model::max('codigo'))->first());
        }

        return TipoClienteResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoCliente::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoCliente $tipo_cliente
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoCliente $tipo_cliente
     */
    public function show(TipoCliente $tipo_cliente)
    {
        TipoClienteResource::withoutWrapping();

        return new TipoClienteResource($tipo_cliente);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoCliente $tipo_cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoCliente $tipo_cliente)
    {
        $input = $request->all();
        $result = $tipo_cliente->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoCliente $tipo_cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoCliente $tipo_cliente)
    {
        $result = $tipo_cliente->delete();

        return response(['deleted' => $result], 204);
    }
}
