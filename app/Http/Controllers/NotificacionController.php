<?php

namespace App\Http\Controllers;


use App\Http\Resources\NotificacionResource;
use App\Notificacion;
use Illuminate\Http\Request;

class NotificacionController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Notificacion;

        // return NotificacionResource::collection(parent::coleccion($request, $model));
        return NotificacionResource::collection(Notificacion::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Notificacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Notificacion $notificacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Notificacion $notificacion
     */
    public function show(Notificacion $notificacion)
    {
        NotificacionResource::withoutWrapping();

        return new NotificacionResource($notificacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Notificacion $notificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notificacion $notificacion)
    {
        $input = $request->all();
        $result = $notificacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notificacion $notificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notificacion $notificacion)
    {
        $result = $notificacion->delete();

        return response(['deleted' => $result], 204);
    }
}
