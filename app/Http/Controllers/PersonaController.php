<?php

namespace App\Http\Controllers;

use App\Http\Resources\PersonaResource;
use App\Persona;
use Illuminate\Http\Request;

class PersonaController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Persona;
            

        if ($request->has('nuevo-codigo')) {
            return new PersonaResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return PersonaResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Persona::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Persona $persona
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Persona $persona
     */
    public function show(Persona $persona)
    {
        PersonaResource::withoutWrapping();

        return new PersonaResource($persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Persona $persona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Persona $persona)
    {
        $input = $request->all();
        $result = $persona->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persona $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persona $persona)
    {
        $result = $persona->delete();

        return response(['deleted' => $result], 204);
    }
}
