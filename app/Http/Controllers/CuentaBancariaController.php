<?php

namespace App\Http\Controllers;

use App\CuentaBancaria;
use App\Http\Resources\CuentaBancariaResource;
use Illuminate\Http\Request;

class CuentaBancariaController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new CuentaBancaria;
            

        if ($request->has('nuevo-codigo')) {
            return new CuentaBancariaResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return CuentaBancariaResource::collection(parent::coleccion($request, $model));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = CuentaBancaria::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\CuentaBancaria $cuenta_bancaria
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\CuentaBancaria $cuenta_bancaria
     */
    public function show(CuentaBancaria $cuenta_bancaria)
    {
        CuentaBancariaResource::withoutWrapping();

        return new CuentaBancariaResource($cuenta_bancaria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CuentaBancaria $cuenta_bancaria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CuentaBancaria $cuenta_bancaria)
    {
        $input = $request->all();
        $result = $cuenta_bancaria->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CuentaBancaria $cuenta_bancaria
     * @return \Illuminate\Http\Response
     */
    public function destroy(CuentaBancaria $cuenta_bancaria)
    {
        $result = $cuenta_bancaria->delete();

        return response(['deleted' => $result], 204);
    }
}
