<?php

namespace App\Http\Controllers;

use App\CentroCosto;
use App\Http\Resources\CentroCostoResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CentroCostoController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new CentroCosto;

        if ($request->has('nuevo-codigo')) {
            return $model::nuevoCodigo();
        }

        return CentroCostoResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        abort_if($input['unidad_productiva_id'] != Auth::user()->unidadProductiva->id, 400, 'La unidad productiva seleccioanada no coincide con la base de datos.');
        $result = CentroCosto::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\CentroCosto $centrocosto
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\CentroCosto $centrocosto
     */
    public function show(CentroCosto $centro_costo)
    {
        CentroCostoResource::withoutWrapping();

        return new CentroCostoResource($centro_costo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CentroCosto $centrocosto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CentroCosto $centro_costo)
    {
        $input = $request->all();
        $result = $centro_costo->update($input);//TODO Ver msj de fallo


        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CentroCosto $centrocosto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CentroCosto $centro_costo)
    {
        $result = $centro_costo->delete();

        return response(['deleted' => $result], 204);
    }
}
