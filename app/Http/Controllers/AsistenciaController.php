<?php

namespace App\Http\Controllers;

use App\Asistencia;
use App\Http\Resources\AsistenciaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AsistenciaController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Asistencia;

        return AsistenciaResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'centro_costo_id' => 'required',
            'tipo_hora_id' => 'required',
            'persona_id' => 'required',
            'fecha' => 'required|date',
            'entrada.hora' => 'bail|required|min:0|max:23',
            'salida.hora' => 'bail|min:0|max:23',
            'entrada.minuto' => 'bail|required|min:0|max:59',
            'salida.minuto' => 'bail|min:0|max:59',
        ]);

        $asistencia = $request->all();

        abort_if($asistencia['persona_id'] != Auth::user()->id, 403, 'No puedes asignar horas a terceros');  //TODO: Si el usuario no es administrador, validar persona_id == auth.id

        if (!is_null($asistencia['salida']) && (!is_null($asistencia['salida']['hora']) || !is_null($asistencia['salida']['hora']))) {
            abort_if($asistencia['entrada']['hora'] > $asistencia['salida']['hora'] && $asistencia['salida']['hora'] != 0, 400, 'La hora de entrada no puede ser mayor a la de salida.');
            $result = Asistencia::create([
                'unidad_productiva_id' => Auth::user()->unidadProductiva->id,
                'persona_id' => $asistencia['persona_id'],
                'centro_costo_id' => $asistencia['centro_costo_id'],
                'tipo_hora_id' => $asistencia['tipo_hora_id'],
                'fecha' => $asistencia['fecha'],
                'hora_entrada' => $asistencia['entrada']['hora'],
                'hora_salida' => $asistencia['salida']['hora'],
                'minuto_entrada' => $asistencia['entrada']['minuto'],
                'minuto_salida' => $asistencia['salida']['minuto'],
                'total' => Asistencia::calcularHorasDia($asistencia),
            ]);
        } else {
            $result = Asistencia::create([
                'unidad_productiva_id' => Auth::user()->unidadProductiva->id,
                'persona_id' => $asistencia['persona_id'],
                'centro_costo_id' => $asistencia['centro_costo_id'],
                'tipo_hora_id' => $asistencia['tipo_hora_id'],
                'fecha' => $asistencia['fecha'],
                'hora_entrada' => $asistencia['entrada']['hora'],
                'minuto_entrada' => $asistencia['entrada']['minuto'],
            ]);
        }

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Asistencia $asistencia
     */
    public function show(Asistencia $asistencia)
    {
        AsistenciaResource::withoutWrapping();

        return new AsistenciaResource($asistencia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asistencia $asistencia)
    {
        $asistencia = $request->all();
        if (!is_null($asistencia['salida'])) {
            abort_if($asistencia['entrada']['hora'] > $asistencia['salida']['hora'] && $asistencia['salida']['hora'] != 0, 400, 'La hora de entrada no puede ser mayor a la de salida.');
            $asistencia['total'] = Asistencia::calcularHorasDia($asistencia);
        }
        $result = $asistencia->update($asistencia);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asistencia $asistencia
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Asistencia $asistencia)
    {
        $result = $asistencia->delete();

        return response(['deleted' => $result], 204);
    }

    public function ultima()
    {
        $ultima = Auth::user()->obtenerUltimaAsistencia();
        abort_if($ultima == null, 400, 'No posees asistencias previas.');
        return new AsistenciaResource($ultima);
    }

    public function personal(Request $request, \App\Persona $persona)
    {
        $model = new Asistencia;
        $where = [
            'persona_id' => $persona->id
        ];
        return AsistenciaResource::collection(parent::coleccion($request, $model, $where));
    }

    public function centroCosto(Request $request, \App\CentroCosto $centro_costo)
    {
        $model = new Asistencia;
        $where = [
            'centro_costo_id' => $centro_costo->id
        ];
        return AsistenciaResource::collection(parent::coleccion($request, $model, $where));
    }
}
