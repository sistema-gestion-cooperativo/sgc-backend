<?php

namespace App\Http\Controllers;

use App\Http\Resources\InstitucionPensionResource;
use App\InstitucionPension;
use Illuminate\Http\Request;

class InstitucionPensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InstitucionPensionResource::collection(InstitucionPension::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = InstitucionPension::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\InstitucionPension $institucion_pension
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\InstitucionPension $institucion_pension
     */
    public function show(InstitucionPension $institucion_pension)
    {
        InstitucionPensionResource::withoutWrapping();

        return new InstitucionPensionResource($institucion_pension);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InstitucionPension $institucion_pension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstitucionPension $institucion_pension)
    {
        $input = $request->all();
        $result = $institucion_pension->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InstitucionPension $institucion_pension
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstitucionPension $institucion_pension)
    {
        $result = $institucion_pension->delete();

        return response(['deleted' => $result], 204);
    }
}
