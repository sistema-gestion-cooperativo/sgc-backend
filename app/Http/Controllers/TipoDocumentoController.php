<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoDocumentoResource;
use App\TipoDocumento;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TipoDocumentoResource(TipoDocumento::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoDocumento::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoDocumento $tipo_documento
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoDocumento $tipo_documento
     */
    public function show(TipoDocumento $tipo_documento)
    {
        TipoDocumentoResource::withoutWrapping();

        return new TipoDocumentoResource($tipo_documento);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoDocumento $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoDocumento $tipo_documento)
    {
        $input = $request->all();
        $result = $tipo_documento->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoDocumento $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoDocumento $tipo_documento)
    {
        $result = $tipo_documento->delete();

        return response(['deleted' => $result], 204);
    }
}
