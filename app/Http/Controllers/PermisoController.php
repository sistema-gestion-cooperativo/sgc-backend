<?php

namespace App\Http\Controllers;

use App\Http\Resources\PermisoResource;
use App\Permiso;
use Illuminate\Http\Request;

class PermisoController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Permiso;
            

        if ($request->has('nuevo-codigo')) {
            return new PermisoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return PermisoResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Permiso::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Permiso $permiso
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Permiso $permiso
     */
    public function show(Permiso $permiso)
    {
        PermisoResource::withoutWrapping();

        return new PermisoResource($permiso);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Permiso $permiso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permiso $permiso)
    {
        $input = $request->all();
        $result = $permiso->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permiso $permiso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permiso $permiso)
    {
        $result = $permiso->delete();

        return response(['deleted' => $result], 204);
    }
}
