<?php

namespace App\Http\Controllers;

use App\Http\Resources\VacacionResource;
use App\Vacacion;
use Illuminate\Http\Request;

class VacacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VacacionResource::collection(Vacacion::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Vacacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacacion $vacacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Vacacion $vacacion
     */
    public function show(Vacacion $vacacion)
    {
        return new VacacionResource($vacacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Vacacion             $vacacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacacion $vacacion)
    {
        $input = $request->all();
        $result = $vacacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vacacion $vacacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacacion $vacacion)
    {
        $result = $vacacion->delete();

        return response(['deleted' => $result], 204);
    }
}
