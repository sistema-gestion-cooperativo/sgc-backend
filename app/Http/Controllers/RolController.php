<?php

namespace App\Http\Controllers;

use App\Http\Resources\RolResource;
use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RolResource::collection(Rol::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Rol::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Rol $rol
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Rol $rol
     */
    public function show(Rol $rol)
    {
        RolResource::withoutWrapping();

        return new RolResource($rol);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Rol $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rol $rol)
    {
        $input = $request->all();
        $result = $rol->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rol $rol)
    {
        $result = $rol->delete();

        return response(['deleted' => $result], 204);
    }
}
