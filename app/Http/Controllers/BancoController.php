<?php

namespace App\Http\Controllers;

use App\Banco;
use App\Http\Resources\BancoResource;
use Illuminate\Http\Request;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BancoResource::collection(Banco::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Banco::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Banco $banco
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Banco $banco
     */
    public function show(Banco $banco)
    {
        BancoResource::withoutWrapping();

        return new BancoResource($banco);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Banco $banco
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banco $banco)
    {
        $input = $request->all();
        $result = $banco->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banco $banco
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banco $banco)
    {
        $result = $banco->delete();

        return response(['deleted' => $result], 204);
    }
}
