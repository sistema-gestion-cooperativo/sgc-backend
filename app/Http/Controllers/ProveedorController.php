<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProveedorResource;
use App\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Proveedor;
            

        if ($request->has('nuevo-codigo')) {
            return new ProveedorResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return ProveedorResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Proveedor::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Proveedor $proveedor
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Proveedor $proveedor
     */
    public function show(Proveedor $proveedor)
    {
        ProveedorResource::withoutWrapping();

        return new ProveedorResource($proveedor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Proveedor $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proveedor $proveedor)
    {
        $input = $request->all();
        $result = $proveedor->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proveedor $proveedor)
    {
        $result = $proveedor->delete();

        return response(['deleted' => $result], 204);
    }
}
