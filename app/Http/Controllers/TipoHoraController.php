<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoHoraResource;
use App\TipoHora;
use Illuminate\Http\Request;

class TipoHoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoHoraResource::collection(TipoHora::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoHora::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoHora $tipo_hora
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoHora $tipo_hora
     */
    public function show(TipoHora $tipo_hora)
    {
        return new TipoHoraResource($tipo_hora);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoHora             $tipo_hora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoHora $tipo_hora)
    {
        $input = $request->all();
        $result = $tipo_hora->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoHora $tipo_hora
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoHora $tipo_hora)
    {
        $result = $tipo_hora->delete();

        return response(['deleted' => $result], 204);
    }
}
