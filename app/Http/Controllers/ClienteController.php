<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Http\Resources\ClienteResource;
use Illuminate\Http\Request;

class ClienteController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Cliente;
            

        if ($request->has('nuevo-codigo')) {
            return new ClienteResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return ClienteResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'unidad_productiva_id' => 'required',
            'rut' => 'required|max:10',
            'nombre' => 'min:2',
            'web' => 'active_url',
            'correo' => 'email',
        ]);
//TODO: json_decode contactos, direcciones y validar ambos
        $input = [
            'unidad_productiva_id' => $request->input('unidad_productiva_id'),
            'rut' => $request->input('rut'),
            'nombre' => $request->input('nombre'),
            'nombre_presentacion' => $request->input('nombre_presentacion'),
            'giro' => $request->input('giro'),
            'web' => $request->input('web'),
            'correo' => $request->input('correo'),
            'telefono' => $request->input('telefono'),
            // 'redes_sociales' => $request->input('redes_sociales'),
            'comentarios' => $request->input('comentarios'),
        ];
        $result = Cliente::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Cliente $cliente
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Cliente $cliente
     */
    public function show(Cliente $cliente)
    {
        ClienteResource::withoutWrapping();

        return new ClienteResource($cliente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        $input = $request->all();
        $result = $cliente->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        $result = $cliente->delete();

        return response(['deleted' => $result], 204);
    }
}
