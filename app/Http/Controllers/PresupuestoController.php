<?php

namespace App\Http\Controllers;

use App\Presupuesto;
use App\Http\Resources\PresupuestoResource;
use Illuminate\Http\Request;

class PresupuestoController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Presupuesto;
            

        if ($request->has('nuevo-codigo')) {
            return new PresupuestoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return PresupuestoResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Presupuesto::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Banco $banco
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Banco $banco
     */
    public function show(Banco $banco)
    {
        PresupuestoResource::withoutWrapping();

        return new PresupuestoResource($banco);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Presupuesto $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presupuesto $presupuesto)
    {
        $input = $request->all();
        $result = $presupuesto->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Presupuesto $presupuesto
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Presupuesto $presupuesto)
    {
        $result = $presupuesto->delete();

        return response(['deleted' => $result], 204);
    }
}
