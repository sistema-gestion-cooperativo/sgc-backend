<?php

namespace App\Http\Controllers;

use App\Http\Resources\FondoPorRendirResource;
use App\FondoPorRendir;
use Illuminate\Http\Request;

class FondoPorRendirController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new FondoPorRendir;
        

        if ($request->has('nuevo-codigo')) {
            return new FondoPorRendirResource($model->where('codigo', $model::max('codigo'))->first());
        }

        return FondoPorRendirResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = FondoPorRendir::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\FondoPorRendir $fondo_por_rendir
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\FondoPorRendir $fondo_por_rendir
     */
    public function show(FondoPorRendir $fondo_por_rendir)
    {
        return new FondoPorRendirResource($fondo_por_rendir);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\FondoPorRendir             $fondo_por_rendir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FondoPorRendir $fondo_por_rendir)
    {
        $input = $request->all();
        $result = $fondo_por_rendir->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FondoPorRendir $fondo_por_rendir
     * @return \Illuminate\Http\Response
     */
    public function destroy(FondoPorRendir $fondo_por_rendir)
    {
        $result = $fondo_por_rendir->delete();

        return response(['deleted' => $result], 204);
    }
}
