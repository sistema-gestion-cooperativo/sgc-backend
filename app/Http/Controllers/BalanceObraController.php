<?php

namespace App\Http\Controllers;

use App\BalanceObra;
use App\Http\Resources\BalanceObraResource;
use App\Http\Resources\Resource;
use Illuminate\Http\Request;

class BalanceObraController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new BalanceObra;
            

        if ($request->has('nuevo-codigo')) {
            return new BalanceObraResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return BalanceObraResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = BalanceObra::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\BalanceObra $balance_obra
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\BalanceObra $balance_obra
     */
    public function show(BalanceObra $balance_obra)
    {
        return new BalanceObraResource($balance_obra);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\BalanceObra             $balance_obra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BalanceObra $balance_obra)
    {
        $input = $request->all();
        $result = $balance_obra->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BalanceObra $balance_obra
     * @return \Illuminate\Http\Response
     */
    public function destroy(BalanceObra $balance_obra)
    {
        $result = $balance_obra->delete();

        return response(['deleted' => $result], 204);
    }
}
