<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoNotificacionResource;
use App\EstadoNotificacion;
use Illuminate\Http\Request;

class EstadoNotificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoNotificacionResource::collection(EstadoNotificacion::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoNotificacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoNotificacion $estadoNotificacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoNotificacion $estadoNotificacion
     */
    public function show(EstadoNotificacion $estadoNotificacion)
    {
        return new EstadoNotificacionResource($estadoNotificacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoNotificacion             $estadoNotificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoNotificacion $estadoNotificacion)
    {
        $input = $request->all();
        $result = $estadoNotificacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoNotificacion $estadoNotificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoNotificacion $estadoNotificacion)
    {
        $result = $estadoNotificacion->delete();

        return response(['deleted' => $result], 204);
    }
}
