<?php

namespace App\Http\Controllers;

use App\Http\Resources\InstitucionPrevisionResource;
use App\InstitucionPrevision;
use Illuminate\Http\Request;

class InstitucionPrevisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InstitucionPrevisionResource::collection(InstitucionPrevision::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = InstitucionPrevision::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\InstitucionPrevision $institucion_prevision
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\InstitucionPrevision $institucion_prevision
     */
    public function show(InstitucionPrevision $institucion_prevision)
    {
        InstitucionPrevisionResource::withoutWrapping();

        return new InstitucionPrevisionResource($institucion_prevision);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InstitucionPrevision $institucion_prevision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstitucionPrevision $institucion_prevision)
    {
        $input = $request->all();
        $result = $institucion_prevision->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InstitucionPrevision $institucion_prevision
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstitucionPrevision $institucion_prevision)
    {
        $result = $institucion_prevision->delete();

        return response(['deleted' => $result], 204);
    }
}
