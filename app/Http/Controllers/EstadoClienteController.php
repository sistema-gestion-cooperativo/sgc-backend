<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoClienteResource;
use App\EstadoCliente;
use Illuminate\Http\Request;

class EstadoClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoClienteResource::collection(EstadoCliente::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoCliente::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoCliente $estadoCliente
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoCliente $estadoCliente
     */
    public function show(EstadoCliente $estadoCliente)
    {
        return new EstadoClienteResource($estadoCliente);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoCliente             $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCliente $estadoCliente)
    {
        $input = $request->all();
        $result = $estadoCliente->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCliente $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCliente $estadoCliente)
    {
        $result = $estadoCliente->delete();

        return response(['deleted' => $result], 204);
    }
}
