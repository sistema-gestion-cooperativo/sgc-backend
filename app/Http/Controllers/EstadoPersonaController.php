<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoPersonaResource;
use App\EstadoPersona;
use Illuminate\Http\Request;

class EstadoPersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoPersonaResource::collection(EstadoPersona::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoPersona::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoPersona $estadoPersona
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoPersona $estadoPersona
     */
    public function show(EstadoPersona $estadoPersona)
    {
        return new EstadoPersonaResource($estadoPersona);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoPersona             $estadoPersona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoPersona $estadoPersona)
    {
        $input = $request->all();
        $result = $estadoPersona->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoPersona $estadoPersona
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoPersona $estadoPersona)
    {
        $result = $estadoPersona->delete();

        return response(['deleted' => $result], 204);
    }
}
