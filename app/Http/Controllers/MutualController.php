<?php

namespace App\Http\Controllers;

use App\Http\Resources\MutualResource;
use App\Mutual;
use Illuminate\Http\Request;

class MutualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MutualResource::collection(Mutual::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Mutual::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Mutual $mutual
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Mutual $mutual
     */
    public function show(Mutual $mutual)
    {
        MutualResource::withoutWrapping();

        return new MutualResource($mutual);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Mutual $mutual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mutual $mutual)
    {
        $input = $request->all();
        $result = $mutual->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mutual $mutual
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mutual $mutual)
    {
        $result = $mutual->delete();

        return response(['deleted' => $result], 204);
    }
}
