<?php

namespace App\Http\Controllers;

use App\Contrato;
use App\Http\Resources\ContratoResource;
use Illuminate\Http\Request;

class ContratoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Contrato;
            

        if ($request->has('nuevo-codigo')) {
            return new ContratoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return ContratoResource::collection(parent::coleccion($request, $model));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Contrato::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Contrato $contrato
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Contrato $contrato
     */
    public function show(Contrato $contrato)
    {
        ContratoResource::withoutWrapping();

        return new ContratoResource($contrato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Contrato $contrato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contrato $contrato)
    {
        $input = $request->all();
        $result = $contrato->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contrato $contrato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contrato $contrato)
    {
        $result = $contrato->delete();

        return response(['deleted' => $result], 204);
    }
}
