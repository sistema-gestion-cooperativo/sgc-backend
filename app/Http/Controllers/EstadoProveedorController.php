<?php

namespace App\Http\Controllers;

use App\Http\Resources\EstadoProveedorResource;
use App\EstadoProveedor;
use Illuminate\Http\Request;

class EstadoProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstadoProveedorResource::collection(EstadoProveedor::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = EstadoProveedor::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EstadoProveedor $estadoProveedor
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\EstadoProveedor $estadoProveedor
     */
    public function show(EstadoProveedor $estadoProveedor)
    {
        return new EstadoProveedorResource($estadoProveedor);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EstadoProveedor             $estadoProveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoProveedor $estadoProveedor)
    {
        $input = $request->all();
        $result = $estadoProveedor->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoProveedor $estadoProveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoProveedor $estadoProveedor)
    {
        $result = $estadoProveedor->delete();

        return response(['deleted' => $result], 204);
    }
}
