<?php

namespace App\Http\Controllers;

use App\Http\Resources\TipoCotizacionResource;
use App\TipoCotizacion;
use Illuminate\Http\Request;

class TipoCotizacionController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new TipoCotizacion;
        

        if ($request->has('nuevo-codigo')) {
            return new TipoCotizacionResource($model->where('codigo', $model::max('codigo'))->first());
        }

        return TipoCotizacionResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = TipoCotizacion::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TipoCotizacion $tipo_cotizacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\TipoCotizacion $tipo_cotizacion
     */
    public function show(TipoCotizacion $tipo_cotizacion)
    {
        TipoCotizacionResource::withoutWrapping();
        return new TipoCotizacionResource($tipo_cotizacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoCotizacion $tipo_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoCotizacion $tipo_cotizacion)
    {
        $input = $request->all();
        $result = $tipo_cotizacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoCotizacion $tipo_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoCotizacion $tipo_cotizacion)
    {
        $result = $tipo_cotizacion->delete();

        return response(['deleted' => $result], 204);
    }
}
