<?php

namespace App\Http\Controllers;

use App\HaberDescuento;
use App\Http\Resources\HaberDescuentoResource;
use App\Http\Resources\Resource;
use Illuminate\Http\Request;

class HaberDescuentoController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new HaberDescuento;
            

        if ($request->has('nuevo-codigo')) {
            return new HaberDescuentoResource($model->where('codigo', $model::max('codigo'))->first());
        }  
        
        return HaberDescuentoResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = HaberDescuento::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\HaberDescuento $haberdescuento
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\HaberDescuento $haberdescuento
     */
    public function show(HaberDescuento $haberdescuento)
    {
        return new HaberDescuentoResource($haberdescuento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\HaberDescuento             $haberdescuento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HaberDescuento $haberdescuento)
    {
        $input = $request->all();
        $result = $haberdescuento->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HaberDescuento $haberdescuento
     * @return \Illuminate\Http\Response
     */
    public function destroy(HaberDescuento $haberdescuento)
    {
        $result = $haberdescuento->delete();

        return response(['deleted' => $result], 204);
    }
}
