<?php

namespace App\Http\Controllers;

use App\Http\Resources\ActaResource;
use App\Http\Resources\ListaActasResource;
use App\Acta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActaController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Acta;

        if ($request->has('nuevo-codigo')) {
            return $model::nuevoCodigo();
        }
        
        return ListaActasResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['acuerdos'] = json_encode($input['acuerdos'], JSON_HEX_QUOT);
        $input['temas_proxima_asamblea'] = json_encode($input['temas_proxima_asamblea'], JSON_HEX_QUOT);
        $input['compromisos'] = json_encode($input['compromisos'], JSON_HEX_QUOT);
        //HACK TODO: No es consistente con ptra solucipones
        $input['hora_inicio'] = $input['hora_inicio']['hora'] . ':' . $input['hora_inicio']['minuto'];
        $input['hora_termino'] = $input['hora_termino']['hora'] . ':' . $input['hora_termino']['minuto'];
        //HACK: Esto debería llegar por frontend
        $input['unidad_productiva_id'] = Auth::user()->unidadProductiva->id;
        $input['temas'] = json_encode($input['observaciones']);
        $result = Acta::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Acta $acta
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Acta $acta
     */
    public function show(Acta $acta)
    {
        return new ActaResource($acta);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Acta             $acta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Acta $acta)
    {
        $input = $request->all();
        $result = $acta->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Acta $acta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Acta $acta)
    {
        $result = $acta->delete();

        return response(['deleted' => $result], 204);
    }
}
