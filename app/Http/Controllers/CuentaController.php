<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Http\Resources\CuentaResource;
use Illuminate\Http\Request;

class CuentaController extends ListableController
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Cuenta;
            
        if ($request->has('nuevo-codigo')) {
            // return new CuentaResource($model->where('codigo', $model::max('codigo'))->first());
            // $model->nuevoCodigo();
        }
        if ($request->has('cuenta-mayor')) {
            $cuentaMayor = $request->query('cuenta-mayor');
            return CuentaResource::collection($model->{$cuentaMayor}());
        }
        
        return CuentaResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Cuenta::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Cuenta $cuenta
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Cuenta $cuenta
     */
    public function show(Cuenta $cuenta)
    {
        CuentaResource::withoutWrapping();

        return new CuentaResource($cuenta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Cuenta $cuenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuenta $cuenta)
    {
        $input = $request->all();
        $result = $cuenta->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuenta $cuenta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuenta $cuenta)
    {
        $result = $cuenta->delete();

        return response(['deleted' => $result], 204);
    }
}
