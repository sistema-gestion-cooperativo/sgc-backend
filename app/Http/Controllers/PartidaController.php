<?php

namespace App\Http\Controllers;

use App\Http\Resources\PartidaResource;
use App\Partida;
use Illuminate\Http\Request;

class PartidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PartidaResource::collection(Partida::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $result = Partida::create($input);

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Partida $partida
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Partida $partida
     */
    public function show(Partida $partida)
    {
        return new PartidaResource($partida);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Partida             $partida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partida $partida)
    {
        $input = $request->all();
        $result = $partida->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partida $partida
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partida $partida)
    {
        $result = $partida->delete();

        return response(['deleted' => $result], 204);
    }
}
