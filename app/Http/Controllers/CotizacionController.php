<?php

namespace App\Http\Controllers;

use App\Cotizacion;
use App\Partida;
use App\Http\Resources\CotizacionResource;
use Illuminate\Http\Request;

class CotizacionController extends ListableController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index(Request $request)
    {
        $model = new Cotizacion;

        if ($request->has('nuevo-codigo')) {
            return $model::nuevoCodigo();
        }

        return CotizacionResource::collection(parent::coleccion($request, $model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $cotizacion = Cotizacion::create([
            'centro_costo_id' => $input['centro_costo_id'],
            'cliente_id' => $input['cliente_id'],
            'cliente' => $input['cliente'],
            'codigo' => $input['codigo'],
            'codigo_modificacion' => $input['codigo_modificacion'],
            'comentarios' => $input['comentarios'],
            'cotizacion_id' => $input['cotizacion_id'],
            'unidad_productiva_id' => $input['unidad_productiva_id'],
            'estado_cotizacion_id' => $input['estado_cotizacion_id'],
            'fecha_envio' => $input['fecha_envio'],
            'fecha_solicitud' => $input['fecha_solicitud'],
            'monto_afecto' => $input['monto_afecto'],
            'monto_exento' => $input['monto_exento'],
            'monto_descuento' => $input['monto_descuento'],
            'monto_iva' => $input['monto_iva'],
            'monto_neto' => $input['monto_neto'],
            'monto_total' => $input['monto_total'],
            'monto_utilidad' => $input['monto_utilidad'],
            'nombre_presentacion' => $input['nombre_presentacion'],
            'porcentaje_descuento' => $input['porcentaje_descuento'],
            'porcentaje_utilidad' => $input['porcentaje_utilidad'],
            'responsable_cotizacion_id' => $input['responsable_cotizacion_id'],
            'responsable_proyecto_id' => $input['responsable_proyecto_id'],
            'tipo_cotizacion_id' => $input['tipo_cotizacion_id'],
            'tipo_documento_id' => $input['tipo_documento_id'],
            'validez' => $input['validez'],
            'valor_cip' => $input['valor_cip'],
            'valor_hora' => $input['valor_hora'],
            'valor_uf' => $input['valor_uf'],
        ]);

        foreach ($input['partidas'] as $partida) {
            // dd($partida);
            $cotizacion->partidas()->create([
                'codigo' => $partida['codigo'],
                'numero' => $partida['numero'],
                'nombre_presentacion' => $partida['nombre_presentacion'],
                'cantidad' => $partida['cantidad'],
                'unidad' => $partida['unidad'],
                'precio' => $partida['precio'],
                'descripcion' => $partida['descripcion'],
                'mano_de_obra' => json_encode($partida['presupuesto']['mano_de_obra']),
                'presupuesto_materiales' => $partida['presupuesto']['materiales'],
                'presupuesto_excedentes' => $partida['presupuesto']['excedentes'],
                'presupuesto_hospedaje' => $partida['presupuesto']['hospedaje'],
                'presupuesto_viatico' => $partida['presupuesto']['viatico'],
                'presupuesto_traslado' => $partida['presupuesto']['traslado'],
                'presupuesto_otros' => $partida['presupuesto']['otros'],
            ]);
        }

        return response($cotizacion, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param \App\Cotizacion $cotizacion
     */
    public function show(Cotizacion $cotizacion)
    {
        CotizacionResource::withoutWrapping();

        return new CotizacionResource($cotizacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cotizacion $cotizacion)
    {

        $input = $request->all();
        abort_if(Cotizacion::where('codigo', $input['codigo'])->first()->centroCosto == null, 403, 'Las cotizaciones asignadas a centros de costo son inmutables');
        $result = $cotizacion->update($input);

        return response(['updated' => $result], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cotizacion $cotizacion)
    {
        $result = $cotizacion->delete();

        return response(['deleted' => $result], 204);
    }
}
