<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AsistenciaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id' => $this->id,
        'entrada' => ['hora' => $this->hora_entrada, 'minuto' => $this->minuto_entrada,],
        'salida' => ['hora' => $this->hora_salida, 'minuto' => $this->minuto_salida,],
        'centro_costo' => $this->centroCosto()->select('id', 'nombre_presentacion')->first(), //TODO
        'tipo_hora' => $this->tipoHora()->select('id', 'nombre_presentacion')->first(),
        'total' => $this->total,
        'fecha' => $this->fecha,
        'persona' => $this->persona()->select('id', 'nombre_presentacion')->first(),
        'entrada_formato' => $this->obtenerEntradaFormato(),
        'salida_formato' => $this->obtenerSalidaFormato(),
        'presentacion' => $this->obtenerPresentacion(),
        ];
    }
}
