<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ActaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $xrequest
     * @return array
     */
    public function toArray($request)
    {
        return [
            'tipo_acta' => $this->tipo()->select('id', 'nombre_presentacion')->first(),
            'numero' => $this->numero,
            'fecha' => $this->fecha,
            'hora_inicio' => $this->hora_inicio,
            'hora_termino' => $this->hora_termino,
            'observaciones' => $this->observaciones,
            'actuario' => $this->actuario()->select('id', 'nombre_presentacion')->first(),
            'moderador' => $this->moderador()->select('id', 'nombre_presentacion')->first(),
            'temas' => $this->temas,
            'acuerdos' => $this->acuerdos,
            'compromisos' => $this->compromisos,
            'temas_proxima_asamblea' => $this->temas_proxima_asamblea,
            'titulos_tema' => $this->titulos_tema,
            'titulos_acuerdo' => $this->titulos_acuerdo,
            'titulos_compromiso' => $this->titulos_compromiso,
            'palabras_clave' => $this->palabras_clave,
        ];
    }
}
