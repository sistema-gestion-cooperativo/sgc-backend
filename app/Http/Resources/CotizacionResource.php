<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CotizacionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
    //   'id' => $this->id,
    //   'codigo' => $this->codigo,
    //   'nombre_presentacion' => $this->nombre_presentacion,
    //   'centro_costo' => $this->centroCosto()->select('id', 'nombre_presentacion')->first(),
    //   'cliente' => $this->cliente()->select('id', 'nombre_presentacion')->first(),
    //   'fecha_solicitud' => $this->fecha_solicitud,
    //   'fecha_envio' => $this->fecha_envio,
    //   'validez' => $this->validez,
    //   'tipo_cotizacion' => $this->tipo()->select('id', 'nombre_presentacion')->first(),
    //   'estado_cotizacion' => $this->estado()->select('id', 'nombre_presentacion')->first(),
    //   'tipo_documento' => $this->tipoDocumento()->select('id', 'nombre_presentacion')->first(),
    //   'monto_exento' => $this->monto_exento,
    //   'monto_afecto' => $this->monto_afecto,
    //   'neto' => $this->neto,
    //   'iva' => $this->iva,
    //   'total' => $this->total,
    //   'responsable' => $this->responsable()->select('id', 'nombre_presentacion')->first(),
    //   'codigo_modificacion' => $this->codigo_modificacion,
    //   'cotizacion_anterior' => $this->cotizacionAnterior()->select('id', 'nombre_presentacion')->first(),
    //   'comentarios' => $this->comentarios,
    //   'created_at' => $this->created_at,
    //   'updated_at' => $this->updated_at,
    // ];
    }
}
