<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ListaActasResource extends Resource
{
    
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_acta' => $this->tipo()
                ->select('id', 'nombre_presentacion')->first(),
            'numero' => $this->numero,
            'fecha' => $this->fecha,
            'hora_inicio' => $this->hora_inicio,
            'hora_termino' => $this->hora_termino,
            'actuario' => $this->actuario()
                ->select('id', 'nombre_presentacion')->first(),
            'moderador' => $this->moderador()
                ->select('id', 'nombre_presentacion')->first(),
            'titulos_tema' => $this->titulos_tema,
            'titulos_acuerdo' => $this->titulos_acuerdo,
            'titulos_compromiso' => $this->titulos_compromiso,
            'palabras_clave' => $this->palabras_clave,
        ];
    }
}
