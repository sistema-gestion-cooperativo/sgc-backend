<?php

namespace App\Http\Resources;

use App\Menu;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class PerfilResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $persona = Auth::user();

        return [
            'unidad_productiva' => $persona->unidadProductiva,
            'persona' => [
                'id' => $persona->id,
                'rut' => $persona->rut,
                'rol' => $persona->rol,
                'nombre_presentacion' => $persona->nombre_presentacion,
                'nombre_usuario' => $persona->nombre_usuario,
                'nombres' => $persona->nombres,
                'primer_apellido' => $persona->primer_apellido,
                'segundo_apellido' => $persona->segundo_apellido,
                'segundo_apellido' => $persona->segundo_apellido,
                'foto_perfil' => $persona->foto_perfil,
            ],
            'menu' => Menu::obtenerMenu(),
        ];
    }
}
