<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GastoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo' => $this->codigo,
            'folio' => $this->folio,
            'fecha' => $this->fecha,
            'periodo_mes' => $this->periodo_mes,
            'periodo_ano' => $this->periodo_ano,
            'monto_exento' => $this->monto_exento,
            'monto_afecto' => $this->monto_afecto,
            'monto_neto' => $this->monto_neto,
            'monto_iva' => $this->monto_iva,
            'monto_total' => $this->monto_total,
            'centro_costo' => $this->centroCosto()->select('id', 'nombre_presentacion')->first(),
        ];
    }
}
