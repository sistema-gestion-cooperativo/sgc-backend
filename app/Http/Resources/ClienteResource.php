<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ClienteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'codigo' => $this->codigo,
            'rut' => $this->rut,
            'nombre' => $this->nombre,
            'nombre_presentacion' => $this->nombre_presentacion,
            'giro' => $this->giro,
            'descuento' => $this->descuento,
            'calificaciones' => $this->calificaciones,
            'numero_cotizaciones' => $this->numero_cotizaciones,
        ];
    }
}
