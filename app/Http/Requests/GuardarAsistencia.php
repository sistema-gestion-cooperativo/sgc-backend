<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarAsistencia extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('crear-asistencia') || $this->user()->id === $this->get('persona_id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                '*.centro_costo_id' => 'bail|required',
                '*.unidad_productiva_id' => 'required',
                '*.fecha' => 'required',
                '*.hora_entrada' => 'required',
                '*.hora_salida' => 'required',
                '*.minuto_entrada' => 'required',
                '*.minuto_salida' => 'required',
                '*.persona_id' => 'required',
            ];
    }
}
