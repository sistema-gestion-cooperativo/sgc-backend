<?php
/**
 * Persona
 *
 * Describe el modelo Persona, que cumple la función de Usuario y Trabajador
 */

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Persona extends Authenticatable
{
    use HasApiTokens, Notifiable, Searchable;

    protected $table = 'personas';

    protected $fillable = [
        'rut',
        'nombre_usuario',
        'contrasena',
        'rol_id',
        'nombres',
        'primer_apellido',
        'segundo_apellido',
        'nombre_presentacion',
        'correos',
        'telefonos',
        'direcciones',
        'fecha_nacimiento',
        'estado_civil',
        'foto_perfil',
        'comentarios',
    ];

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'personas';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        // static::addGlobalScope(new UnidadProductivaScope);
    }

    //Métodos para configurar Usuario
    public function findForPassport($username)
    {
        return $this->where('nombre_usuario', $username)->first();
    }

    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->contrasena);
    }

    public function getAuthPassword()
    {
        return $this->contrasena;
    }

    //Relaciones
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function asistencias()
    {
        return $this->hasMany('App\Asistencia');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoPersona', 'estado_persona_id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }

    public function calcularSueldoHoras()
    {
        // $this->asistencias()->whereBetween('fecha', $inicio_mes, $fin_mes)
        // return 0;
    }

    public function obtenerUltimaAsistencia()
    {
        return $this->asistencias()->orderBy('fecha', 'desc')->orderBy('hora_entrada', 'desc')->first();
    }
}
