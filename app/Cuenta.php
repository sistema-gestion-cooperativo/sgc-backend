<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Cuenta extends Model
{
    use Searchable;

    protected $table = 'cuentas';

    protected $fillable = [
        'unidad_productiva_id',
        'nombre_plan',
        'codigo',
        'cuenta_mayor',
        'subcuenta',
        'registro'
    ];

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'cuentas';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoCuenta', 'estado_cuenta_id');
    }

    //Scopes
    public function scopeIngresos($query)
    {
        return $query->where('cuenta_mayor', 'INGRESOS');
    }
    public function scopeActivos($query)
    {
        return $query->where('cuenta_mayor', 'ACTIVOS');
    }
    public function scopeCostos($query)
    {
        return $query->where('cuenta_mayor', 'COSTOS');
    }
    public function scopePatrimonio($query)
    {
        return $query->where('cuenta_mayor', 'PATRIMONIO');
    }
    public function scopePasivos($query)
    {
        return $query->where('cuenta_mayor', 'PASIVOS');
    }
}
