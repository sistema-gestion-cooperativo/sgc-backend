<?php

namespace App;

use App\Events\AsistenciaSaving;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\Exception;

class Asistencia extends Model
{
    protected $fillable = [
        'unidad_productiva_id',
        'persona_id',
        'hora_entrada',
        'hora_salida',
        'minuto_entrada',
        'minuto_salida',
        'fecha',
        'centro_costo_id',
        'total',
        'tipo_hora_id',
    ];

    /**
     * Precarga las relaciones indicadas.
     *
     * @var array
     */
    protected $with = [
        'tipo_hora',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saving' => AsistenciaSaving::class,
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    // public function setTotalAttribute($value) {
    //     $this->attributes['total'] = $asistencia->calcularHorasDia();
    // }

    public static function calcularHorasDia($asistencia)
    {
        if (!isset($asistencia['entrada']['hora']) ||
            !isset($asistencia['salida']['minuto']) ||
            !isset($asistencia['entrada']['hora']) ||
            !isset($asistencia['salida']['minuto'])) {
            throw new Exception('Faltan datos de horario para calcular las horas totales'); //TODO: Elegir Exception (está tirandolo por Phpspreadsheet)
        }
        $segundos_entrada = 3600 * $asistencia['entrada']['hora'] + 60 * $asistencia['entrada']['minuto'];
        $segundos_salida = 3600 * $asistencia['salida']['hora'] + 60 * $asistencia['salida']['hora'];

        if ($segundos_entrada > $segundos_salida) {
            throw new Exception('La hora de entrada excede a la hora de salida');
        }
        return round(($segundos_salida - $segundos_entrada) / 3600, 2);
    }

    public function calcularValorActualHora()
    {
        //TODO: Debe tomar en cuenta el tipo de hora, el sueldo pactado
        return 0;
    }
    public function calcularValorHistoricoHora()
    {
        //TODO: Debe tomar en cuenta la fecha de la asistencia (ara ver el valor hora)
        return 0;
    }

    public function obtenerEntradaFormato()
    {
        return str_pad($this->hora_entrada, 2, '0', STR_PAD_LEFT) . ':' . str_pad($this->minuto_entrada, 2, '0', STR_PAD_LEFT);
    }

    public function obtenerSalidaFormato()
    {
        return str_pad($this->hora_salida, 2, '0', STR_PAD_LEFT) . ':' . str_pad($this->minuto_salida, 2, '0', STR_PAD_LEFT);
    }

    public function obtenerPresentacion()
    {
        return $this->fecha . ' ' . $this->obtenerEntradaFormato() . '-' . $this->obtenerSalidaFormato() . ' : ' . $this->centroCosto->nombre_presentacion;
    }

  // Relations
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto', 'centro_costo_id');
    }
    public function tipoHora()
    {
        return $this->belongsTo('App\TipoHora');
    }
    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
}
