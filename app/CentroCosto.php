<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class CentroCosto extends Model
{
    use Searchable;
    protected $table = 'centro_costos';

    protected $fillable = [
        'unidad_productiva_id',
        'codigo',
        'nombre_presentacion',
        'estado_centro_costo_id',
        'responsable_id'
    ];

    /**
    * Precarga las relaciones indicadas.
    *
    * @var array
    */
    protected $with = [
        'estado',
        'responsable'
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public static function nuevoCodigo()
    {
        $ultimoCodigo = CentroCosto::max('codigo');
        $proximoCodigo = ++$ultimoCodigo;
        return ["codigo" => $proximoCodigo];
    }


    // Relations
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function asistencias()
    {
        return $this->hasMany('App\Asistencia');
    }

    public function gastos()
    {
        return $this->hasMany('App\Gasto');
    }

    public function cotizaciones()
    {
        return $this->hasMany('App\Cotizacion');
    }

    public function cheques()
    {
        return $this->hasMany('App\Cheque');
    }

    public function fondosPorRendir()
    {
        return $this->hasMany('App\FondoPorRendir');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoCentroCosto', 'estado_centro_costo_id');
    }

    public function responsable()
    {
        return $this->belongsTo('App\Persona', 'responsable_id');
    }

    public function ultimaCotizacion()
    {
        return $this->belongsTo('App\Cotizacion', 'ultima_cotizacion_id');
    }

    //TODO: Si tiene adicional_de_centro_costo_id, mostrar el último adicional y no el primero

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'centro_costos';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        if (!is_null($this->estado)) {
            $array['estado'] = $this->estado->nombre_presentacion;
        }
        if (!is_null($this->responsale)) {
            $array['responsale'] = $this->responsale->nombre_presentacion;
        }

        return $array;
    }
}
