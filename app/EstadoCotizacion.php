<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCotizacion extends Model
{
  protected $table = 'estados_cotizacion';

  protected $fillable = [
    'nombre_presentacion',
  ];
}
