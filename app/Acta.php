<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class Acta extends Model
{
    protected $table = 'actas';
    protected $fillable = [
        'unidad_productiva_id',
        'tipo_acta_id',
        'actuario_id',
        'moderador_id',
        'numero',
        'fecha',
        'hora_inicio',
        'hora_termino',
        'temas',
        'acuerdos',
        'compromisos',
        'temas_proxima_asamblea',
        'titulos_tema',
        'titulos_acuerdo',
        'titulos_compromiso',
        'palabras_clave',
        'observaciones',
        'comentarios',
    ];

    /**
     * Precarga las relaciones indicadas.
     *
     * @var array
     */
    protected $with = [
        'tipo',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public static function nuevoCodigo()
    {
        $ultimoCodigo = Acta::max('numero');
        $proximoCodigo = ++$ultimoCodigo; //Si max devuelve false, sumarle uno lo volvera un nÃºmero 1 (false == 0)
        return ["codigo" => $proximoCodigo];
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function tipo()
    {
        return $this->belongsTo('App\TipoActa');
    }

    public function actuario()
    {
        return $this->belongsTo('App\Persona', 'actuario_id');
    }
    public function moderador()
    {
        return $this->belongsTo('App\Persona', 'moderador_id');
    }

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'actas';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        if (!is_null($this->moderador)) {
            $array['moderador'] = $this->moderador->nombre_presentacion;
        }
        if (!is_null($this->actuario)) {
            $array['actuario'] = $this->actuario->nombre_presentacion;
        }
        if (!is_null($this->tipo)) {
            $array['tipo'] = $this->tipo->nombre_presentacion;
        }

        return $array;
    }
}
