<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacacion extends Model
{
	protected $table = 'vacaciones';
    protected $fillable = [
        'fecha_inicio','fecha_termino','dias_habiles'
    ];
}
