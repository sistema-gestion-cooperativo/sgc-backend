<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CajaCompensacion extends Model
{
    protected $table = 'cajas_compensacion';

    protected $fillable = [
      'codigo',
      'nombre_presentacion'
    ];
}
