<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class HaberDescuento extends Model
{
    protected $table = 'haberes_descuentos';

    protected $fillable = [
        'unidad_productiva_id', 'tipo', 'nombre_presentacion', 'imponible', 'monto', 'porcentaje',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
}
