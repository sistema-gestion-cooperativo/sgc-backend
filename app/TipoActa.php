<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoActa extends Model
{
	protected $table = 'tipos_acta';
    protected $fillable = [
        'nombre_presentacion'
    ];

}
