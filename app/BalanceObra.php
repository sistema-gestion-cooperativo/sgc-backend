<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

/**
 * El BalanceObra es una LookupTable que se actualiza cada vez que se solicita
 * Es invariable, esta actualización consiste en que se crea uno nuevo
 * Los balances están relacionados a un CentroCosto de manera unívoca
 * El último balance se consigue con el centro de costo y máxima fecha de balance
 * Se puede comprobar si está actualizado contando los gastos relacionados al centro de costos
 */
class BalanceObra extends Model
{
    protected $table = 'balances_obra';
    protected $fillable = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto');
    }

    public function responsableProyecto()
    {
        return $this->belongsTo('App\Persona', 'responsable_proyecto_id');
    }

    public function responsableCotizacion()
    {
        return $this->belongsTo('App\Persona', 'responsable_cotizacion_id');
    }


    public function calcularBalance()
    {
        $totalGasto = \App\Gasto::where('centro_costo_id', $this->centroCosto()->id)
            ->reduce(function ($carry, $item) {
                return $carry + $item->monto_total;
            }, 0);
        // $totalManoObra = \App\Asistencia::where('centro_costo_id', $this->centroCosto()->id); //TODO: Calcular el valor por tipo de hora, tomando en cuenta la fecha de inicio proyecto para usar los mismos valores de esa fecha
        $totalManoObra = 0;
        $totalHospedaje = 0;
        $totalViatico = 0;
        $totalTraslado = 0;
        $totalOtros = 0;
        $totalCostoIndirecto = 0;

        $presupuestoCostoIndirecto = 0;
        $presupuestoMateriales = $this->ultimaCotizacion->presupuestoMateriales();
        $presupuestoManoDeObra = $this->ultimaCotizacion->presupuestoManoDeObra();
        $presupuestoExcedentes = $this->ultimaCotizacion->presupuestoExcedentes();
        $presupuestoHospedaje = $this->ultimaCotizacion->presupuestoHospedaje();
        $presupuestoViatico = $this->ultimaCotizacion->presupuestoViatico();
        $presupuestoTraslado = $this->ultimaCotizacion->presupuestoTraslado();
        $presupuestoOtros = $this->ultimaCotizacion->presupuestoOtros();

        $balance = [
            'materiales' => $presupuestoMateriales - $totalMateriales,
            'mano_obra' => $presupuestoManoObra - $totalManoObra,
            // 'excedentes' => $presupuestoExcedentes - $totalExcedentes,
            'hospedaje' => $presupuestoHospedaje - $totalHospedaje,
            'viatico' => $presupuestoViatico - $totalViatico,
            'traslado' => $presupuestoTraslado - $totalTraslado,
            'otros' => $presupuestoOtros - $totalOtros,
            'costo_indirecto' => $presupuestoCostoIndirecto - $totalCostoIndirecto,
        ];

        return $balance;
    }
}
