<?php

namespace App\Events;

use App\Asistencia;

class AsistenciaSaving
{
    // use SerializesModels;

    public $asistencia;

    /**
     * Create a new event instance.
     *
     * @param  \App\Asistencia  $asistencia
     * @return void
     */
    public function __construct(Asistencia $asistencia)
    {
//TODO: Recalcualr balances?

        $this->asistencia = $asistencia;
    }


}
