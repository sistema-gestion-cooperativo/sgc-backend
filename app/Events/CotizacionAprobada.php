<?php

namespace App\Events;

use App\Cotizacion;
use App\BalanceObra;
use App\CentroCosto;

class CotizacionAprobada
{

    public $cotizacion;

    /**
     * Create a new event instance.
     *
     * @param  \App\Cotizacion  $cotizacion
     * @return void
     */
    public function __construct(Cotizacion $cotizacion)
    {
        $centro_costo = [
            'unidad_productiva_id' => $cotizacion['unidad_productiva_id'],
            'codigo' => $cotizacion['codigo'],
            'nombre_presentacion' => $cotizacion['nombre_presentacion'],
            'responsable_id' => $cotizacion['responsable_faena_id'],
        ];

        CentroCosto::create($centro_costo);
        
        $this->cotizacion = $cotizacion;
    }


}