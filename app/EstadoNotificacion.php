<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoNotificacion extends Model
{
	protected $table = 'estados_notificacion';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
