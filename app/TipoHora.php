<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoHora extends Model
{
    protected $table = 'tipos_hora';

    protected $fillable = [
        'nombre_presentacion','valor_hora','descripcion','comentarios'
    ];

    public function asistencias()
    {
        return $this->hasMany('App\Asistencia');
    }
}
