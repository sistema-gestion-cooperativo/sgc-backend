<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCuentaBancaria extends Model
{
	protected $table = 'estados_cuenta_bancaria';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
