<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCliente extends Model
{
	protected $table = 'estados_cliente';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
