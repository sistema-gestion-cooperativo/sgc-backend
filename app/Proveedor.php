<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Proveedor extends Model
{
    use Searchable;

    protected $table = 'proveedores';

    protected $fillable = [
        'rut',
        'nombre',
        'nombre_presentacion',
        'giro',
        'web',
        'correo',
        'telefono',
        'redes_sociales',
        'datos_adicionales',
        'calificaciones',
        'comentarios',
    ];

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'proveedores';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoProveedor', 'estado_proveedor_id');
    }
}
