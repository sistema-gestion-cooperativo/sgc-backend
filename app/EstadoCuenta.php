<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCuenta extends Model
{
	protected $table = 'estados_cuenta';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
