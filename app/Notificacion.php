<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = 'notificaciones';
    protected $fillable = [
        'unidad_productiva_id',
        'nombre_presentacion',
        'color',
        'icono',
        'contenido',
        'comentarios',
        'estado_notificacion_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoNotificacion', 'estado_notificacion_id');
    }
}
