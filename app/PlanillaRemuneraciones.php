<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanillaRemuneraciones extends Model
{
    // Nombre
    public function obtenerPlanillaRemuneraciones()
    {
        $planilla = [
            'horas_habiles_mes' => '',
            'sueldo_mes' => '',
        ];

        foreach (Personas::all() as $persona) {
            $planilla['planilla'][] = [
                'persona_rut' => $this->persona()->rut,
                'persona_nombre_presentacion' => $this->persona()->nombre_presentacion,
                'persona_tipo' => $this->persona()->tipo,
                'sueldo_base' => $this->persona()->contrato->sueldo_base,
                'sueldo_mes' => $this->persona()->contrato->sueldo_mes,
                'horas_normal' => $this->persona()->horas_normal(),
                'horas_region' => $this->persona()->horas_region(),
                'horas_noche' => $this->persona()->horas_noche(),
                'horas_total' => $this->persona()->horas_total(),
                'persona_tipo' => $this->persona()->tipo,
            ];
        }
    }
    public function obtenerPlanillaLiquidaciones()
    {
        return [
            'persona_rut' => $this->persona()->rut,
            'persona_nombre_presentacion' => $this->persona()->nombre_presentacion,
// Calidad Trabajador
            'persona_tipo' => $this->persona()->tipo,
// Horas contrato
            'contrato_horas' => $this->persona()->contrato_vigente()->horas_semana,
//  Sueldo Base(Codigo)
            'contrato_sueldo_base' => $this->persona()->contrato_vigente()->sueldo_base,
//  Gratificación contrato (Código)
            'contrato_gratificacion' => $this->persona()->contrato_vigente()->gratificacion,
//  Retiro excedentes (socio)
//  Bono alimentacion contrato
//  bono locomoción contrato
// AFP
            'institucion_pension' => $this->persona()->institucion_pension,
// Institucion Salud
            'institucion_prevision' => $this->persona()->institucion_prevision,
// if(isapre) Pactado en Isapre (UF)
            'uf_prevision' => $this->persona()->factor_prevision,
// if(fonasa)
            'factor_prevision' => IndicadorEconomico::factor_prevision(),
// Seg. Cesantía trabaj
// Seg. Cesantía Coope
// Mutual
            'mutual' => $this->unidadProductiva()->mutual,
// Seguro de Invalidez y Sobrevivencia (SIS)
// Total Días Trab.
            'horas_trabajo' => $this->persona()->horas_trabajo,
            'horas_extra' => $this->persona()->horas_extra,
            'horas_inasistencia' => $this->persona()->horas_inasistencia,
// Dias licencia
// Ingreso Ley Coop
// Bono Alimenación
// Bono Desg. Herramienta
// Bono Movilización
// TOTAL NO IMPONIBLE
// Sueldo base
// Gratificación
// Retiro Ant. Excedentes
// Horas Extras3
// bono producción (codigo)
// Descuentos por atrasos
// Retiro Ant. Adicional (socios)
// TOTAL IMPONIBLE
//  TOTAL HABERES
//  TOTAL Imposiciones
//  TOTAL convencionales
//  TOTAL descuenteso
//  liquido
//  Sueldo base para cesantía
//  Mutual 3,5%
//  SIS           1,15%
//  Seguro Cesantía 3%
//  TOTAL PREVIRED
// polla
// a PAGAR
        ];
    }
}
