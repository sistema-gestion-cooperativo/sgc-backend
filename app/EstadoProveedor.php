<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoProveedor extends Model
{
	protected $table = 'estados_proveedor';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
