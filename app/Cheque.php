<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected $table = 'cheques';
    protected $fillable = [
        'numero', 'monto', 'comentarios'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }
    
    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }
}
