<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCentroCosto extends Model
{
    protected $table = 'estados_centro_costo';
    protected $fillable = [
        'nombre_presentacion'
    ];
}
