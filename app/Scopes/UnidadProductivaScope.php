<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class UnidadProductivaScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Comprueba que la solicitud sea hecha vía web para no interferir con el agregar índices a Laravel Scout
        if (array_key_exists('REQUEST_METHOD', $_SERVER)) {
            $unidad_productiva = Auth::user()->unidadProductiva;

            abort_if(
                is_null($unidad_productiva),
                403,
                'Unidad productiva no seleccionada'
            );

            $builder->whereHas('unidadProductiva', function ($q) use ($unidad_productiva) {
                $q->where('id', $unidad_productiva->id);
            });
        }
    }
}
