<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadProductiva extends Model
{
    protected $table = 'unidades_productivas';
    protected $fillable = [
        'rut',
        'nombre_presentacion',
        'giro',
        'web',
        'correos',
        'telefonos',
        'redes_sociales',
        'comentarios',
        'representante_legal_nombres',
        'representante_legal_primer_apellido',
        'representante_legal_segundo_apellido',
        'representante_legal_rut'
    ];
}
