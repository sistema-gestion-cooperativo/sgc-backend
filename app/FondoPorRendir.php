<?php

namespace App;

use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;

class FondoPorRendir extends Model
{
    protected $table = 'fondos_por_rendir';
    protected $fillable = [
        'numero', 'monto', 'diferencia', 'comentarios'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }
    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto');
    }
}
