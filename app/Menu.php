<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{
    static public function obtenerMenu()
    { //TODO: Obtener por usuario registrado y cargo
        return json_decode(DB::table('menus')->first()->menu);
    }
}
