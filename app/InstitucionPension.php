<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstitucionPension extends Model
{
    protected $table = 'instituciones_pensiones';

    protected $fillable = [
        'codigo',
        'nombre_presentacion',
        'factor',
        'fecha_factor',
    ];
}
