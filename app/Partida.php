<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table = 'partidas';
    protected $fillable = [
        'cotizacion_id',
        'codigo',
        'numero',
        'nombre_presentacion',
        'cantidad',
        'unidad',
        'precio',
        'descripcion',
        'presupuesto_materiales',
        'presupuesto_mano_de_obra',
        'presupuesto_excedentes',
        'presupuesto_hospedaje',
        'presupuesto_viatico',
        'presupuesto_traslado',
        'presupuesto_otros',
        'mano_de_obra'
    ];

    public function cotizacion()
    {
        return $this->belongsTo('App\Cotizacion', 'cotizacion_id');
    }
}
