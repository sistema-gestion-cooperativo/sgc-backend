<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoPersona extends Model
{
	protected $table = 'estados_persona';
    protected $fillable = [
        'nombre_presentacion','visible'
    ];
}
