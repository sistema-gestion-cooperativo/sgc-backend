<?php

namespace App;

use App\Scopes\VisibleScope;
use App\Scopes\UnidadProductivaScope;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Cliente extends Model
{
    use Searchable;

    protected $fillable = [
        'unidad_productiva_id',
        'rut',
        'nombre',
        'nombre_presentacion',
        'giro',
        'web',
        'correo',
        'telefono',
        'redes_sociales',
        'datos_adicionales',
        'descuento',
        'calificaciones',
        'comentarios',
    ];

    /**
     * Precarga las relaciones indicadas.
     *
     * @var array
     */
    protected $with = [
        'tipo',
    ];

    /**
     * Atributos no guardados en la base de datos que se calculan a partir de accesadores.
     * Se agregan al arreglo del modelo y su representación JSON.
     * Respetan los arreglos $visible y $hidden.
     *
     * @var array
     */
    // protected $appends = [
    //     'numero_cotizaciones',
    //     'numero_cotizaciones_aprobadas',
    //     'numero_cotizaciones_rechazadas',
    // ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VisibleScope);
        static::addGlobalScope(new UnidadProductivaScope);
    }

    public function unidadProductiva()
    {
        return $this->belongsTo('App\UnidadProductiva');
    }

    public function cotizaciones()
    {
        return $this->hasMany('App\Cotizacion');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoCliente', 'estado_cliente_id');
    }

    public function tipo()
    {
        return $this->belongsTo('App\TipoCliente', 'tipo_cliente_id');
    }


    // Campos
    public function getNumeroCotizacionesAttribute()
    {
        return $this->cotizaciones->count();
    }

    public function getNumeroCotizacionesAprobadasAttribute()
    {
        return $this->cotizaciones->where('estado', 2)->count();
    }

    public function getNumeroCotizacionesRechazadasAttribute()
    {
        return $this->cotizaciones->where('estado', 3)->count();
    }

    /**
     * Get the scout index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'clientes';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        if (!is_null($this->tipo)) {
            $array['tipo'] = $this->tipo->nombre_presentacion;
        }
        if (!is_null($this->estado)) {
            $array['estado'] = $this->estado->nombre_presentacion;
        }

        return $array;
    }
}
