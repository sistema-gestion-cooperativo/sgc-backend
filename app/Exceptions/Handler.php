<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
      'password',
      'password_confirmation',
      'contrasena',
      'contrasena_confirmacion',
    ];

    /**
     * Report or log an exception.
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /*
        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response(['message' => __('errors.not_found')], 404);
        } elseif ($exception instanceof \Illuminate\Database\QueryException) {
            switch($exception->getCode()) {
                case 23505:
                    $message = __('errors.model_already_exists');
                    break;
                case 23503:
                    $message = __('errors.related_model_not_exists');
                    break;
                case 23502:
                    $message = __('errors.missing_information');
                    break;
                default:
                    $message = __('errors.database', ['code' => $exception->getCode()]);
                    break;
            }
            return response(['message' => $message], 409);
        } elseif ($exception instanceof Exception) {
            return response(array_merge(['message' => __('errors.database', ['code' => $exception->getCode()])], $exception->getTrace()), 500);
        }
        */

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return response()->json(['message' => __('errors.validation'), 'errors' => $exception->validator->getMessageBag()], 422);
        }

        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }
}
