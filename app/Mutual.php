<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutual extends Model
{
    protected $table = 'mutuales';

    protected $fillable = [
      'codigo',
      'nombre_presentacion'
    ];
}
