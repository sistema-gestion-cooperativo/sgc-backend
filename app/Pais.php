<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'paises';
    public $timestamps = false;
    protected $fillable = [
        'nombre','iso2','iso3','prefijo_telefonico'
    ];
}
