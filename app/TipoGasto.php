<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoGasto extends Model
{
	protected $table = 'tipos_gasto';
    protected $fillable = [
        'codigo',
        'nombre_presentacion'
    ];
}
