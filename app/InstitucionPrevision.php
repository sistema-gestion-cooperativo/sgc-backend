<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstitucionPrevision extends Model
{
    protected $table = 'instituciones_previsionales';

    protected $fillable = [
      'codigo',
      'nombre_presentacion',
    ];
}
