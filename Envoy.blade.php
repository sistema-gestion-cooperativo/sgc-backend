@servers(['web' => 'deployer@coenergia.cl'])

@setup
    $repository = 'git@gitlab.com:cristobalramos/enter-erp.git';
    $releases_dir = '/var/www/sgc/releases';
    $app_dir = '/var/www/sgc';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    colan_repositorio
    run_composer
    update_symlinks
@endstory

@task('colan_repositorio')
    echo 'Clonando repositorio'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Iniciando despliege ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -o --optimize-autoloader --no-dev
    php artisan config:cache
    php artisan route:cache
@endtask

@task('update_symlinks')
    echo 'Creando symlink de carpeta storage'
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Enlazando archivo .env'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Enlazando último lanzamiento'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask
