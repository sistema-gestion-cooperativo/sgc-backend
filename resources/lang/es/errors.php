<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Mensajes generales
    |--------------------------------------------------------------------------
    |
    | Acá se agregan los mensajes generales de validación, que involucran al
    | formulario completo.
    |
    */
    'validation' => 'Los datos enviados tienen errores.',
    'not_found' => 'No encontrado.',
    'forbidden' => 'Prohibido.',
    'missing_information' => 'No están todos los datos necesarios',
    'model_already_exists' => 'El concepto ya existe.',
    'related_model_not_exists' => 'No existe alguno de los conceptos relacionados.',
    'database' => 'Error :code en la base de datos.',
    'unexpected' => 'Error inesperado. Copie el siguiente código y contacte a administración. :code.',
];
