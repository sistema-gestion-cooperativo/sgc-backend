<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

  'password' => 'Ña contraseña debe tener al menos seis caracteres.',
  'reset' => 'Tu contraseña de ha actualizado.',
  'sent' => 'Hemos enviado el correo para reestablecer tu contraseña.',
  'token' => 'Este token de reestablecimiento no es valido.',
  'user' => 'No existe un usuario con ese correo.',

];
