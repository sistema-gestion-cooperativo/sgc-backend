<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

  'failed' => 'Estas credenciales no coinciden con nuestra base de datos.',
  'throttle' => 'Demasidos intentos fallidos. Por favor intenta en :seconds segundos.',

];
