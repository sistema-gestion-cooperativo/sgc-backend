<?php

namespace Tests\Feature;

use Tests\TestCase;

class CotizacionTest extends TestCase
{
    //N = 579

    public function testApiPuedeObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1); //TODO: Cambiar por una persona de prueba que no sea de esa empresa

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/cotizacion');

        $this->assertEquals(200, $response->status());
    }

    public function testPuedoFiltrarPorVisiblesEnUnidadProductiva()
    {
        // select count(*) from cotizaciones where estado_cotizacion_id=3
        //TODO: A futuro la visibilidad por defecto de un estado debería estar en la base de datos como un campo booleano
        $cotizacionesWhere = \App\Cotizacion::where('unidad_productiva_id', 1)->whereNotIn('estado_cotizacion_id', [3])->count();
        $cotizacionesFiltradas = \App\Cotizacion::count();
        $this->assertEquals($cotizacionesWhere, $cotizacionesFiltradas);
    }
}
