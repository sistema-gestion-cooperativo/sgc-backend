<?php

namespace Tests\Feature;

use Tests\TestCase;

class TipoCotizacionTest extends TestCase
{

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/tipo_cotizacion');

        $this->assertEquals(200, $response->status());
    }
}
