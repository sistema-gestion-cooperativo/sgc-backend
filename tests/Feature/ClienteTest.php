<?php

namespace Tests\Feature;

use Tests\TestCase;

class ClienteTest extends TestCase
{
    //N = 100

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/cliente');

        $this->assertEquals(200, $response->status());
    }
}
