<?php

namespace Tests\Feature;

use Tests\TestCase;

class InstitucionPensionTest extends TestCase
{
    public function testPuedoObtenerListaCompleta()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/institucion_pension');

        $this->assertEquals(200, $response->status());
    }
}
