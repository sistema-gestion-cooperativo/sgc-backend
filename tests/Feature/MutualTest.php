<?php

namespace Tests\Feature;

use Tests\TestCase;

class MutualTest extends TestCase
{
    public function testPuedoObtenerListaCompleta()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/mutual');

        $this->assertEquals(200, $response->status());
    }
}
