<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class MenuTest extends TestCase
{
    public function testLaEstructuraDelRegistroEsCorrecta()
    {
        $columns = DB::getSchemaBuilder()->getColumnListing('menus');
        $requiredColumns = [
            'id',
            'menu',
            'created_at',
            'updated_at',
        ];
        
        $this->assertSame(array_diff($columns, $requiredColumns), array_diff($requiredColumns, $columns));
    }
}
