<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class FondoPorRendirTest extends TestCase
{

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/fondo_por_rendir');

        $this->assertEquals(200, $response->status());
    }

    public function testLaEstructuraDelRegistroEsCorrecta()
    {
        $columns = DB::getSchemaBuilder()->getColumnListing('fondos_por_rendir');

        $requiredColumns = [
            'id',
            'numero',
            'unidad_productiva_id',
            'centro_costo_id',
            'responsable_id',
            'monto',
            'diferencia',
            'fecha_entrega',
            'fecha_cierre',
            'fecha_devolucion',
            'comentarios',
            'created_at',
            'updated_at',
        ];
        //el true al final activa la flag $canonicalize que ordena los arreglos (si no podría fallar por orden)
        $this->assertEquals($columns, $requiredColumns, 'la tabla no tiene la estructura requerida', 0.0, 10, true);
    }
}