<?php

namespace Tests\Feature;

use Tests\TestCase;

class RolTest extends TestCase
{
    public function testNoPuedoObtenerListaSiNoEstoyIngresado()
    {
        $response = $this->call('GET', '/api/rol');

        $this->assertEquals(401, $response->status());
    }

    public function testPuedoObtenerLista()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/rol');

        $this->assertEquals(200, $response->status());
    }
}
