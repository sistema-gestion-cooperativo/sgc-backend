<?php

namespace Tests\Feature;

use Tests\TestCase;

class PerfilTest extends TestCase
{
    public function testNoPuedoObtenerPerfilSiNoEstoyAutenticado()
    {
        $response = $this->get('/api/perfil');

        $this->assertEquals(401, $response->status());
    }

    public function testPuedoObtenerMiPerfil()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/perfil');

        $this->assertEquals(200, $response->status());
    }
}
