<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProveedorTest extends TestCase
{

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/proveedor');

        $this->assertEquals(200, $response->status());
    }
}
