<?php

namespace Tests\Feature;

use Tests\TestCase;

class GastoTest extends TestCase
{
    //N = 2000

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/gasto');

        $this->assertEquals(200, $response->status());
    }
}
