<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HaberDescuentoTest extends TestCase
{

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/haber_descuento');

        $this->assertEquals(200, $response->status());
    }
}
