<?php

namespace Tests\Feature;

use Tests\TestCase;

class TipoDocumentoPagoTest extends TestCase
{
    public function testPuedoObtenerListaCompleta()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/tipo_documento_pago');

        $this->assertEquals(200, $response->status());
    }
}
