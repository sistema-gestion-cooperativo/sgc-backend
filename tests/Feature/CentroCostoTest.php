<?php

namespace Tests\Feature;

use Tests\TestCase;

class CentroCostoTest extends TestCase
{
    //N = 300
    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/centro_costo');

        $this->assertEquals(200, $response->status());
    }

    public function testElFormatoJsonEsCorrecto()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->json('GET', '/api/centro_costo');

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'unidad_productiva_id',
                    'codigo',
                    'nombre_presentacion',
                    'estado',
                    'adicional_de_centro_costo_id',
                    'responsable_id',
                    'comentarios',
                ]
            ]
        ]);
    }

    // public function testAdministradorPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(2);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }

    // public function testComercialPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(3);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }

    // public function testProduccionPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(4);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }

    // public function testRecursosHumanosPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(5);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }

    // public function testSocioPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(6);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }

    // public function testColaboradorPuedeObtenerListaCompleta()
    // {
    //     $persona = \App\Persona::find(7);

    //     $response = $this->actingAs($persona, 'api')
    //         ->call('GET', '/api/centro_costo');

    //     $this->assertEquals(200, $response->status());
    // }
}
