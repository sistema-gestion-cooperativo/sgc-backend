<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class ActaTest extends TestCase
{
    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/acta');

        $this->assertEquals(200, $response->status());
    }

    public function testLaEstructuraDelRegistroEsCorrecta()
    {
        $columns = DB::getSchemaBuilder()->getColumnListing('actas');

        $requiredColumns = [
            'id',
            'unidad_productiva_id',
            'tipo_acta_id',
            'numero',
            'fecha',
            'hora_inicio',
            'hora_termino',
            'actuario_id',
            'moderador_id',
            'temas',
            'acuerdos',
            'compromisos',
            'temas_proxima_asamblea',
            'titulos_tema',
            'titulos_acuerdo',
            'titulos_compromiso',
            'palabras_clave',
            'observaciones',
            'comentarios',
            'created_at',
            'updated_at',
        ];

        //el true al final activa la flag $canonicalize que ordena los arreglos (si no podría fallar por orden)
        //vendor/phpunit/phpunit/src/Framework/Assert.php
        //PHPUnit v6.5.6: assertEquals($expected, $actual, $message = '', $delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
        $this->assertEquals($columns, $requiredColumns, 'la tabla no tiene la estructura requerida', 0.0, 10, true);
    }
}