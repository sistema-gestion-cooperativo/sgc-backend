<?php

namespace Tests\Feature;

use Tests\TestCase;

class CuentaBancariaTest extends TestCase
{

    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/cuenta_bancaria');

        $this->assertEquals(200, $response->status());
    }
}
