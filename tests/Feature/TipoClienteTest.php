<?php

namespace Tests\Feature;

use Tests\TestCase;

class TipoClienteTest extends TestCase
{
    public function testPuedoObtenerListaSiEsMiUnidadProductiva()
    {
        $persona = \App\Persona::find(1);

        $response = $this->actingAs($persona, 'api')
            ->call('GET', '/api/tipo_cliente');

        $this->assertEquals(200, $response->status());
    }
}
