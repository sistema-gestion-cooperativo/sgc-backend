<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->group(function () {
    Route::get('perfil', 'PerfilController@index');

    Route::resource('periodo', 'PeriodoController');
    Route::resource('unidad_productiva', 'UnidadProductivaController');
    Route::resource('rol', 'RolController');

    Route::resource('centro_costo', 'CentroCostoController');
    Route::resource('presupuesto', 'PresupuestoController');
    Route::resource('cotizacion', 'CotizacionController');
    Route::resource('gasto', 'GastoController');
    Route::resource('balance_obra', 'BalanceObraController');

    Route::resource('persona', 'PersonaController');

    Route::prefix('asistencia')->group(function () {
        Route::resource('/', 'AsistenciaController');
        Route::get('ultima', 'AsistenciaController@ultima');
        Route::get('personal/{persona}', 'AsistenciaController@personal');
        Route::get('centro_costo/{centro_costo}', 'AsistenciaController@centroCosto');
    });

    Route::resource('cliente', 'ClienteController');
    Route::resource('proveedor', 'ProveedorController');
    Route::resource('oportunidad', 'OportunidadController');
    Route::resource('cheque', 'ChequeController');
    Route::resource('fondo_por_rendir', 'FondoPorRendirController');

    Route::resource('cuenta', 'CuentaController');
    Route::resource('cuenta_bancaria', 'CuentaBancariaController');

    Route::resource('notificacion', 'NotificacionController');
    Route::resource('acta', 'ActaController');

    //Route::resource('vacacion', 'VacacionController'); //TODO: Crear modelo y todo.
    //Route::resource('remuneracion', 'RemuneracionController'); //TODO: Crear modelo y todo.
    //TODO: Hacer que el usuario eliga al ingrear con que empresa quiere trabajar
    //aparece un menu dedplegable luego de login. Si tiene solo una empresa, no aparece el menu
    //Si no hay empresas, se le ofrece crear una (mientras tenga los privilegios)
    //si no, se le solicita que contacte al superadmin

    Route::resource('institucion_pension', 'InstitucionPensionController');
    Route::resource('institucion_prevision', 'InstitucionPrevisionController');
    Route::resource('caja_compensacion', 'CajaCompensacionController');
    Route::resource('banco', 'BancoController');
    Route::resource('mutual', 'MutualController');

    Route::resource('tipo_cliente', 'TipoClienteController');
    Route::resource('tipo_hora', 'TipoHoraController');
    Route::resource('tipo_documento', 'TipoDocumentoController');
    Route::resource('tipo_documento_pago', 'TipoDocumentoPagoController');
    Route::resource('tipo_cotizacion', 'TipoCotizacionController');
    Route::resource('tipo_gasto', 'TipoGastoController');
    Route::resource('tipo_acta', 'TipoActaController');
    Route::resource('estado_cotizacion', 'EstadoCotizacionController');
    Route::resource('estado_centro_costo', 'EstadoCentroCostoController');
    Route::get('auxiliar_visual', 'AuxiliarVisualController');
    Route::get('pais', 'PaisController');

    Route::resource('haber_descuento', 'HaberDescuentoController');
});
