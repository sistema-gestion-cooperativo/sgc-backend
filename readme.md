# Sistema de Gestión Cooperativo

[![pipeline status](https://gitlab.com/sistema-gestion-cooperativo/sgc-backend/badges/master/pipeline.svg)](https://gitlab.com/sistema-gestion-cooperativo/sgc-backend/commits/master)
[![coverage report](https://gitlab.com/sistema-gestion-cooperativo/sgc-backend/badges/master/coverage.svg)](https://gitlab.com/sistema-gestion-cooperativo/sgc-backend/commits/master)

## Acerca de SGC

SGC, acrónimo de Sistema de Gestión Cooperativo, es un sistema de gestión empresarial enfocado en cooperativas de trabajo especializadas en la venta de servicios en territorio chileno.
Consiste en una aplicación cuyo **backend** es una REST API y su **frontend** es una SPA online, con posibilidad de ser portada a escritorio.
El sistema gestionará los procesos administrativos, productivos y comerciales de una cooperativa de trabajo enfocada en la venta de servicios: Altas, bajas y modificaciones de Personal, Clientes, Contactos, Cotizaciones, Proyectos, Proveedores y Gastos; creación de informes (liquidaciones de sueldo, formularios fiscales, balances y prebalances), control de Asistencias y Vacaciones.


## Hoja de ruta
Una hoja de ruta es una planificación del desarrollo de un software con los objetivos a corto y largo plazo, organizada en hitos con fechas de consecución de estos objetivos.

### Mínimo Producto Viable
 1. *Centros de costos*: Mediados de Febrero
 2. *Asistencias*: Mediados de Marzo
 3. *Gastos*: Finales de Marzo
 4. *Cotización*: Inicios de Abril
 5. *Balances por obra*: Mediados de Abril
 6. *Clientes y Proveedores*: Finales de Abril
 7. *Balance general*: Finales de Abril
### Adicionales
 8. *Personas*
 9. *Préstamos*
 10. *Vacaciones*
 11. *Remuneraciones*
 12. *Finanzas*
### Opcionales
 13. *Actas*
 14. *Contabilidad*

## Roles de usuario
Cada usuario tendrá roles para evitar que su interacción con el sistema conduzca a fallos

 1. *Trabajador*: Usuario no asociado a la cooperativa, que puede gestionar su Asistencia y recibir Notificaciones.
 2. *Socio*: Usuario asociado a la cooperativa. A los privilegios del trabajador, le suma la capacidad de listar todas las entidades.
 3. *Encargado de Obra*: Socio a cargo de uno o más Proyectos en específico. A los privilegios del socio, le suma la capacidad de crear Gastos, Cotizaciones dependientes de la cotización de su actual Proyecto, listar y modificar la Asistencia de las Personas asignadas a su Proyecto.
 4. *Administración*: Usuario con gran conocimiento en el manejo del sistema con una previa capacitación por parte de la entidad. Encargado de manejar el sistema con gran responsabilidad sobre los criterios de permisos sobre los usuarios.


## Funciones
 - *Roles*: El administrador del sistema gestionará y asignará los Roles a las Personas. También gestionará los Permisos de cada Rol.
 - *Personal*: El administrador podrá gestionar el Personal, creación y término de Contratos, asignar y aprobar Vacaciones, aprobar Remuneraciones y llevar un control de Incidentes por Persona. Las Personas son los Usuarios de este sistema en una relación 1 a 1.
 - *Asistencia*: Todo Trabajador podrá registrar y listar su Asistencia.
 - *Remuneraciones*: Todo Trabajador podrá listar sus Remuneraciones.
 - *Vacaciones*: Todo Trabajador puede solicitar Vacaciones y ver los días que dispone para tal evento.
 - *Clientes*: Administración podrá gestionar Clientes, agendar Reuniones, gestionar Contactos y Cotizaciones por Cliente.
 - *Proyectos*: Producción podrá gestionar Proyectos.
 - *Proveedores*: Producción podrá gestionar Proveedores, agendar Reuniones, gestionar Gastos y Contactos por Proveedor.

## Entidades
 - *Acta*: Las Actas son documentos que resumen lo discutido en una Asamblea. Contienen el número de asamblea, tipo, fecha, hora de inicio, de término, listado de asistentes, actuario, moderador, resumen de palabras, decisiones.
 - *Asistencia*: La Asistencia es un conjunto de horas en una fecha determinada, asociada a un Centro de Costos.
 - *Balance de Obra*: El Balance de Obra es el cálculo del costo y ganancia presupestada por un Proyecto, comparada con lo real.
 - *Centro de costos*: Etiqueta para referenciar un Proyecto, Cuenta o elemento para ser referenciado por Asistencias, Gastos y poder calcular Balances según esto.
 - *Cliente*: Entidad natural o jurídica a la que la Unidad Productiva le vende, o desea vender, productos o servicios.
 - *Cotización*: Documento que contiene las Partidas estimadas de un Proyecto. Incluye el Presupuesto.
 - *Fondo por Rendir*: Cantidad de dinero que se le entrega a una Persona para gastos diversos, generalmente asociados a un solo Centro de Costos.
 - *Gasto*: Compra asociada a un Centro de Costos, efectuada por una Persona.
 - *Haberes y Descuentos*: Efectos que suman o restan un monto a la Remuneración de una Persona.
 - *Partida*: Línea de Cotización que se considera una parte atómica de un Proyecto.
 - *Período*: Etiqueta para referenciar un espacio de tiempo.
 - *Persona*: Trabajador de la Unidad Productiva, a la par que Usuario de SGC.
 - *Proveedor*: Entidad natural o jurídica que vende productos o presta servicios a la Unidad Productiva.
 - *Rol*: Los Roles son las funciones que cumplen las Personas al interior de la Unidad Productiva
 - *Unidad Productiva*: La Unidad Productiva es la asociación de trabajadores. Se puede considerar símil a la empresa o cooperativa.

## Licencia
SGC es un proyecto de software abierto licenciado bajo la licencia [MIT](http://opensource.org/licenses/MIT).
