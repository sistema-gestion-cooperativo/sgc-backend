# TODO
## Servidor
### PHP
- disable_functions =exec,passthru,shell_exec,system,proc_open,popen,curl_exec,curl_multi_exec,parse_ini_file,show_source
- allow_url_fopen=Off
- allow_url_include=Off

## Generales
 - Hacer que los controladores entreguen un error cuando se solicite borrar un registro que no existe.
 - Paginación para controladores plurales
 - JSON:API en todos los resources que lo necesiten
 - Dejar que laravel se encargue de las web request (?)
  - Reconfigurar nginx
  - Crear index en public
## Entidades
- Asistencia
- AuxiliarVisual
- Banco
- CajaCompensacion
- CentroCosto
- Cliente
- Contacto
- Contrato
- Cotizacion
- Cuenta
- CuentaBancaria
- UnidadProductiva
- Gasto
- InstitucionPension
- InstitucionPrevision
- Menu
- Mutual
- Oportunidad
- Periodo
- Permiso
- Persona
- Proveedor
- Proyecto
- Rol
- TipoCliente
- TipoDocumento
- TipoDocumentoPago
- TipoProyecto
- Usuario

## Pendientes 
- Sistema de remuneraciones
- Informe de balances y prebalances de obra
- Plantillas de
 - Contratos
 - Finiquitos
 
 
 ## Archivos cambiados
 - src/Bridge/UserRepository.php: https://github.com/laravel/passport/pull/187