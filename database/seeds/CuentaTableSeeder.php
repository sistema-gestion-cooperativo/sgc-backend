<?php

use App\Cuenta;
use Illuminate\Database\Seeder;

class CuentaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cuentas.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Cuenta::create((array)$obj);
        }
        DB::statement("SELECT setval('cuentas_id_seq', COALESCE((SELECT MAX(id)+1 FROM cuentas), 1), false);");
    }
}