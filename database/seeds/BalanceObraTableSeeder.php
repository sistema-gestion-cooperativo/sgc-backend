<?php

use App\BalanceObra;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BalanceObraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/balances_obra.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            BalanceObra::create($obj);
        }
        DB::statement("SELECT setval('balances_obra_id_seq', COALESCE((SELECT MAX(id)+1 FROM balances_obra), 1), false);");
    }
}