<?php

use App\HaberDescuento;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HaberDescuentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/haberes_descuentos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            HaberDescuento::create((array)$obj);
        }
        DB::statement("SELECT setval('haberes_descuentos_id_seq', COALESCE((SELECT MAX(id)+1 FROM haberes_descuentos), 1), false);");
    }
}