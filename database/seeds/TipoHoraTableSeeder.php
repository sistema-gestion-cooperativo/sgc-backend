<?php

use App\TipoHora;
use Illuminate\Database\Seeder;

class TipoHoraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_hora.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoHora::create((array) $obj);
        }
    }
}