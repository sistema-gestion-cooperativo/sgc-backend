<?php

use App\Persona;
use Illuminate\Database\Seeder;

class PersonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/personas.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Persona::create((array)$obj);
        }
        DB::statement("SELECT setval('personas_id_seq', COALESCE((SELECT MAX(id)+1 FROM personas), 1), false);");
    }
}