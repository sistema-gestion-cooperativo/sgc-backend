<?php

use App\Permiso;
use Illuminate\Database\Seeder;

class PermisoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/permisos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Permiso::create((array)$obj);
        }
        DB::statement("SELECT setval('permisos_id_seq', COALESCE((SELECT MAX(id)+1 FROM permisos), 1), false);");
    }
}