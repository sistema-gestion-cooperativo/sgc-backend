<?php

use App\CajaCompensacion;
use Illuminate\Database\Seeder;

class CajaCompensacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cajas_compensacion.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            CajaCompensacion::create((array)$obj);
        }
        DB::statement("SELECT setval('cajas_compensacion_id_seq', COALESCE((SELECT MAX(id)+1 FROM cajas_compensacion), 1), false);");
    }
}