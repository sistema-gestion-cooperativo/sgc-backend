<?php

use App\Partida;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartidaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/partidas.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Partida::create((array) $obj);
        }
    }
}