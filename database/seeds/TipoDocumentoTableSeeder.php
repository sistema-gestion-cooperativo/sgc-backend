<?php

use App\TipoDocumento;
use Illuminate\Database\Seeder;

class TipoDocumentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_documento.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoDocumento::create((array)$obj);
        }
        DB::statement("SELECT setval('tipos_documento_id_seq', COALESCE((SELECT MAX(id)+1 FROM tipos_documento), 1), false);");
    }
}