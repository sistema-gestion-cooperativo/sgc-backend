<?php

use App\TipoGasto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoGastoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_gasto.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoGasto::create((array) $obj);
        }
    }
}