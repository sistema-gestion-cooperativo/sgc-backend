<?php

use App\EstadoCliente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_cliente.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoCliente::create((array) $obj);
        }
    }
}