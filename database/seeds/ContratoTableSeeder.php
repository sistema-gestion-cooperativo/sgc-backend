<?php

use App\Contrato;
use Illuminate\Database\Seeder;

class ContratoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/contratos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Contrato::create((array)$obj);
        }
        DB::statement("SELECT setval('contratos_id_seq', COALESCE((SELECT MAX(id)+1 FROM contratos), 1), false);");
    }
}