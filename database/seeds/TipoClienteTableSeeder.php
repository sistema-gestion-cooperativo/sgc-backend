<?php

use App\TipoCliente;
use Illuminate\Database\Seeder;

class TipoClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_cliente.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoCliente::create((array)$obj);
        }
        DB::statement("SELECT setval('tipos_cliente_id_seq', COALESCE((SELECT MAX(id)+1 FROM tipos_cliente), 1), false);");
    }
}