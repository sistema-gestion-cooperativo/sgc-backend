<?php

use App\CuentaBancaria;
use Illuminate\Database\Seeder;

class CuentaBancariaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cuentas_bancarias.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            CuentaBancaria::create((array)$obj);
        }
        DB::statement("SELECT setval('cuentas_bancarias_id_seq', COALESCE((SELECT MAX(id)+1 FROM cuentas_bancarias), 1), false);");
    }
}