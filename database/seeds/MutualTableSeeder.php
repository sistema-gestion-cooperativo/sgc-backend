<?php

use App\Mutual;
use Illuminate\Database\Seeder;

class MutualTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/mutuales.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Mutual::create((array)$obj);
        }
        DB::statement("SELECT setval('mutuales_id_seq', COALESCE((SELECT MAX(id)+1 FROM mutuales), 1), false);");
    }
}