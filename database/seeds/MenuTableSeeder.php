<?php

use App\Menu;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_encode([
            "Personal" => [
                "color" => "teal",
                "items" => [
                    [
                        "icono" => "fas fa-columns",
                        "texto" => "Escritorio",
                        "nombre" => "escritorio",
                    ],
                    [
                        "icono" => "fas fa-clock",
                        "texto" => "Asistencia",
                        "nombre" => "asistencias",
                    ],
                    [
                        "icono" => "fas fa-sticky-note",
                        "texto" => "Notas",
                        "nombre" => "notas",
                    ],
                    [
                        "icono" => "fas fa-bell",
                        "texto" => "Notificaciones",
                        "nombre" => "notificaciones",
                    ],
                ],
            ],
            "Comercial" => [
                "color" => "indigo",
                "items" => [
                    [
                        "icono" => "fas fa-handshake",
                        "texto" => "Cotizaciones",
                        "nombre" => "cotizaciones",
                    ],
                    [
                        "icono" => "fas fa-address-book",
                        "texto" => "Clientes",
                        "nombre" => "clientes",
                    ],
                ],
            ],
            "Producción" => [
                "color" => "red",
                "items" => [
                    [
                        "icono" => "fas fa-dot-circle",
                        "texto" => "Centro de Costos",
                        "nombre" => "centros-de-costos",
                    ],
                    [
                        "icono" => "fas fa-truck",
                        "texto" => "Proveedores",
                        "nombre" => "proveedores",
                    ],
                    [
                        "icono" => "fas fa-shopping-cart",
                        "texto" => "Gastos",
                        "nombre" => "gastos",
                    ],
                ],
            ],
            "Recursos Humanos" => [
                "color" => "amber",
                "items" => [
                    [
                        "icono" => "fas fa-users",
                        "texto" => "Personas",
                        "nombre" => "personas",
                    ],
                    [
                        "icono" => "fas fa-suitcase",
                        "texto" => "Vacaciones",
                        "nombre" => "vacaciones",
                    ],
                    [
                        "icono" => "fas fa-credit-card",
                        "texto" => "Remuneraciones",
                        "nombre" => "remuneraciones",
                    ],
                ],
            ],
            "Administración" => [
                "color" => "lime",
                "items" => [
                    [
                        "icono" => "fas fa-calculator",
                        "texto" => "Balances",
                        "nombre" => "balances",
                    ],
                    [
                        "icono" => "fas fa-dollar-sign",
                        "texto" => "Cheques",
                        "nombre" => "cheques",
                    ],
                    [
                        "icono" => "fas fa-list-alt",
                        "texto" => "Fondos por rendir",
                        "nombre" => "fondos-por-rendir",
                    ],
                    [
                        "icono" => "fas fa-money-bill-alt",
                        "texto" => "Préstamos",
                        "nombre" => "prestamos",
                    ],
                ],
            ],
            "Orgánico" => [
                "color" => "light-blue",
                "items" => [
                    [
                        "icono" => "fas fa-archive",
                        "texto" => "Actas",
                        "nombre" => "actas",
                    ],
                ],
            ],
            "Sistema" => [
                "color" => "brown",
                "items" => [
                    [
                        "icono" => "fas fa-user",
                        "texto" => "Usuarios",
                        "nombre" => "usuarios",
                    ],
                    [
                        "icono" => "fas fa-cog",
                        "texto" => "Configuración",
                        "nombre" => "configuracion",
                    ],
                ],
            ],
        ]);
        Menu::create(["menu" => $data   ]);
        DB::statement("SELECT setval('menus_id_seq', COALESCE((SELECT MAX(id)+1 FROM menus), 1), false);");
    }
}