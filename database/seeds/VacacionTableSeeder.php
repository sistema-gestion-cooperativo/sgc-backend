<?php

use App\Vacacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VacacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/vacaciones.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Vacacion::create((array) $obj);
        }
    }
}