<?php

use App\Acta;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/actas.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Acta::create((array) $obj);
        }
        DB::statement("SELECT setval('actas_id_seq', COALESCE((SELECT MAX(id)+1 FROM actas), 1), false);");
    }
}