<?php

use App\Oportunidad;
use Illuminate\Database\Seeder;

class OportunidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/oportunidades.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Oportunidad::create((array)$obj);
        }
        DB::statement("SELECT setval('oportunidades_id_seq', COALESCE((SELECT MAX(id)+1 FROM oportunidades), 1), false);");
    }
}