<?php

use App\Gasto;
use Illuminate\Database\Seeder;

class GastoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/gastos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Gasto::create((array)$obj);
        }
        DB::statement("SELECT setval('gastos_id_seq', COALESCE((SELECT MAX(id)+1 FROM gastos), 1), false);");
    }
}