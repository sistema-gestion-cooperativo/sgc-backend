<?php

use App\EstadoPersona;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoPersonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_persona.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoPersona::create((array) $obj);
        }
    }
}