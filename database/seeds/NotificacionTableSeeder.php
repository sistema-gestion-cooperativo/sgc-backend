<?php

use App\Notificacion;
use Illuminate\Database\Seeder;

class NotificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/notificaciones.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Notificacion::create((array)$obj);
        }
        DB::statement("SELECT setval('notificaciones_id_seq', COALESCE((SELECT MAX(id)+1 FROM notificaciones), 1), false);");
    }
}