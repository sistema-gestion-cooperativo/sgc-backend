<?php

use App\Cheque;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChequeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cheques.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Cheque::create((array) $obj);
        }
    }
}