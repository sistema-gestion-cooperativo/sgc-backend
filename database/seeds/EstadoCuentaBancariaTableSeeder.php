<?php

use App\EstadoCuentaBancaria;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCuentaBancariaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_cuenta_bancaria.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoCuentaBancaria::create((array) $obj);
        }
    }
}