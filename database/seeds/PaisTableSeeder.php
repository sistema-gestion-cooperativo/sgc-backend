<?php

use App\Pais;
use Illuminate\Database\Seeder;

class PaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/paises.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Pais::create((array)$obj);
        }
        DB::statement("SELECT setval('paises_id_seq', COALESCE((SELECT MAX(id)+1 FROM paises), 1), false);");
    }
}