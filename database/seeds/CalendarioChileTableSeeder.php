<?php
use Illuminate\Database\Seeder;

class CalendarioChileTableSeeder extends Seeder
{
    const DIAS = [
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
        'Domingo'
    ];
    const MESES = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    ];
    const ESTACIONES = [
        'Verano',
        'Otoño',
        'Invierno',
        'Primavera'
    ];

    //TODO: Tendré la cantidad de día de la semana al año ¿Así se calcula la semana santa?
    const FERIADOS = [
        '1-1' => [
            'nombre' => 'Año nuevo',
            'tipo' => 'Civil e Irrenunciable'
        ],
        '5-1' => [
            'nombre' => 'Día Internacional de los Trabajadores',
            'tipo' => 'Civil e Irrenunciable'
        ],
        '5-21' => [
            'nombre' => 'Día de las Glorias Navales',
            'tipo' => 'Civil'
        ],
        '6-26' => [
            'nombre' => 'San Pedro y San Pablo',
            'tipo' => 'Religioso'
        ],
        '7-16' => [
            'nombre' => 'Día de la Virgen del Carmen',
            'tipo' => 'Religioso'
        ],
        '8-15' => [
            'nombre' => 'Asunción de la Virgen',
            'tipo' => 'Religioso'
        ],
        '9-18' => [
            'nombre' => 'Independencia Nacional',
            'tipo' => 'Civil e Irrenunciable'
        ],
        '9-19' => [
            'nombre' => 'Día de las Glorias del Ejército',
            'tipo' => 'Civil e Irrenunciable'
        ],
        '10-9' => [
            'nombre' => 'Encuentro de Dos Mundos',
            'tipo' => 'Civil'
        ],
        '10-27' => [
            'nombre' => 'Día de las Iglesias Evangélicas y Protestantes',
            'tipo' => 'Religioso'
        ],
        '11-1' => [
            'nombre' => 'Día de Todos los Santos',
            'tipo' => 'Religioso'
        ],
        '12-8' => [
            'nombre' => 'Inmaculada Concepción',
            'tipo' => 'Religioso'
        ],
        '12-25' => [
            'nombre' => 'Navidad',
            'tipo' => 'Religioso e Irrenunciable'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inicio = DateTime::createFromFormat('j-n-Y', '1-1-2010');
        $fin = DateTime::createFromFormat('j-n-Y', '1-1-2030');
        $dias = (int)$inicio->diff($fin)->format('%a');

        for ($i = 0; $i <= $dias; $i++) {
            $fecha = $inicio->format('Ymd');
            $temp1 = clone $inicio;
            $temp2 = clone $inicio;
            $previo_habil = $temp1->modify('-1 weekday')->format('Ymd');
            $proximo_habil = $temp1->modify('+1 weekday')->format('Ymd');
            $primer_dia_semana = $temp2->modify('-7 weekday')->format('Ymd');
            $ultimo_dia_semana = $temp2->modify('+7 weekday')->format('Ymd');
            $es_feriado = array_key_exists($inicio->format('n-j'), self::FERIADOS);
            $es_findesemana = !($inicio->format('w') >= 0 && $inicio->format('w') <= 5);
            $dias_mes = $inicio->format('t');
            $primer_dia_mes = DateTime::createFromFormat('Ymd', $inicio->format('Ym01'));
            $ultimo_dia_mes = DateTime::createFromFormat('Ymd', $inicio->format('Ymt'));
            // var_dump($primer_dia_mes);
            $arreglo_hff = $this->arregloDiasHabilesFeriadosFinDeSemanaEntre($primer_dia_mes, $ultimo_dia_mes);
            $habiles_mes = $arreglo_hff[0];
            $feriados_mes = $arreglo_hff[2];

            if ($es_feriado && array_key_exists($inicio->format('n-j'), self::FERIADOS)) {
                $nombre_feriado = self::FERIADOS[$inicio->format('n-j')]['nombre'];
                $tipo_feriado = self::FERIADOS[$inicio->format('n-j')]['tipo'];
            } elseif ($es_feriado) {
                $nombre_feriado = 'Sin datos';
                $tipo_feriado = '';
            }

            $result = DB::table('calendario_chile')->insert(
                [
                    'fecha' => $fecha,
                    'ano' => $inicio->format('Y'),
                    'numero_mes' => $inicio->format('n'),
                    'numero_dia' => $inicio->format('j'),
                    // 'estacion' => obtenerEstacion(),
                    'nombre_dia' => self::DIAS[$inicio->format('w')],
                    'nombre_mes' => self::MESES[$inicio->format('n') - 1],
                    'dia_semana' => $inicio->format('N'),
                    'dia_ano' => $inicio->format('z') + 1,
                    'semana_ano' => $inicio->format('W'),
                    // 'semana_mes' => $inicio->format(''),
                    'primer_dia_semana' => $primer_dia_semana,
                    'ultimo_dia_semana' => $ultimo_dia_semana,
                    // 'dia_semana_mes' =>,
                    // 'dia_semana_ano' =>,
                    'es_feriado' => $es_feriado,
                    'es_habil' => !$es_feriado && !$es_findesemana,
                    'es_dia_semana' => !$es_findesemana,
                    'nombre_feriado' => $nombre_feriado,
                    'tipo_feriado' => $tipo_feriado,
                    'feriados_mes' => $feriados_mes,
                    'habiles_mes' => $habiles_mes,
                    'previo_habil' => $previo_habil,
                    'proximo_habil' => $proximo_habil,
                    'dias_mes' => $dias_mes,
                ]
            );
            // echo $inicio->format('Y-m-d') . " - " . $result . "\n";
            $inicio->add(new DateInterval('P1D'));
        }
    }

    private function arregloDiasHabilesFeriadosFinDeSemanaEntre(DateTime $from, DateTime $to)
    {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = [
            '1-1',
            '5-1',
            '5-21',
            '6-26',
            '7-16',
            '8-15',
            '9-18',
            '9-19',
            '10-9',
            '10-27',
            '11-1',
            '12-8',
            '12-25',
        ];

        $to->modify('+1 day');
        $interval = new DateInterval('P1D');
        $periods = new DatePeriod($from, $interval, $to);

        $habiles = 0;
        $feriados = 0;
        $fin_de_semana = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) {
                $fin_de_semana++;
            }
            if (in_array($period->format('n-j'), $holidayDays)) {
                $feriados++;
                continue;
            }
            $habiles++;
        }
        return [$habiles, $fin_de_semana, $feriados];
    }
}
