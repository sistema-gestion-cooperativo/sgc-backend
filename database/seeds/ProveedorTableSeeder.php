<?php

use App\Proveedor;
use Illuminate\Database\Seeder;

class ProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/proveedores.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Proveedor::create((array)$obj);
        }
        DB::statement("SELECT setval('proveedores_id_seq', COALESCE((SELECT MAX(id)+1 FROM proveedores), 1), false);");
    }
}