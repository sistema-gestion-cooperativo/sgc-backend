<?php

use App\TipoDocumentoPago;
use Illuminate\Database\Seeder;

class TipoDocumentoPagoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_documento_pago.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoDocumentoPago::create((array)$obj);
        }
        DB::statement("SELECT setval('tipos_documento_pago_id_seq', COALESCE((SELECT MAX(id)+1 FROM tipos_documento_pago), 1), false);");
    }
}