<?php

use App\EstadoCentroCosto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCentroCostoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_centro_costo.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoCentroCosto::create((array) $obj);
        }
    }
}