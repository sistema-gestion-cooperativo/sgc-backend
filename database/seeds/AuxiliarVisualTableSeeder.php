<?php

use App\AuxiliarVisual;
use Illuminate\Database\Seeder;

class AuxiliarVisualTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/auxiliares_visuales.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            AuxiliarVisual::create((array)$obj);
        }
    }
}