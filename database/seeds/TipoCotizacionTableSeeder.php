<?php

use App\TipoCotizacion;
use Illuminate\Database\Seeder;

class TipoCotizacionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $json = File::get('database/data/tipos_cotizacion.json');
    $data = json_decode($json);
    foreach ($data as $obj) {
      TipoCotizacion::create((array) $obj);
    }
    DB::statement("SELECT setval('tipos_cotizacion_id_seq', COALESCE((SELECT MAX(id)+1 FROM tipos_cotizacion), 1), false);");
  }
}
