<?php

use App\EstadoNotificacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoNotificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_notificacion.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoNotificacion::create((array) $obj);
        }
    }
}