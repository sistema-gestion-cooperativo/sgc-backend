<?php

use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the given connection from the given path.
     *
     * @param  array|string  $class
     * @param  bool  $silent
     * @return $this
     */
    public function call($class, $silent = false)
    {
        $inicio = microtime(1);
        $classes = Arr::wrap($class);

        foreach ($classes as $class) {
            if ($silent === false && isset($this->command)) {
                $this->command->getOutput()->writeln("<info>Seeding:</info> $class");
            }

            $this->resolve($class)->__invoke();
        }
        if ($silent === false && isset($this->command)) {
            $this->command->getOutput()->writeln(round((microtime(1) - $inicio), 2));
        }
        return $this;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inicio = microtime(1);

        $this->call(CalendarioChileTableSeeder::class);
        $this->call(UnidadProductivaTableSeeder::class);
        $this->call(RolTableSeeder::class);
        $this->call(PermisoTableSeeder::class);

        $this->call(PaisTableSeeder::class);

        $this->call(TipoCotizacionTableSeeder::class);
        $this->call(TipoHoraTableSeeder::class);    
        $this->call(TipoClienteTableSeeder::class);
        $this->call(TipoDocumentoTableSeeder::class);
        $this->call(TipoDocumentoPagoTableSeeder::class);
        $this->call(TipoGastoTableSeeder::class);
        $this->call(TipoActaTableSeeder::class);
        $this->call(EstadoCentroCostoTableSeeder::class);
        $this->call(EstadoClienteTableSeeder::class);
        $this->call(EstadoCotizacionTableSeeder::class);
        $this->call(EstadoCuentaBancariaTableSeeder::class);
        $this->call(EstadoNotificacionTableSeeder::class);
        $this->call(EstadoPersonaTableSeeder::class);
        $this->call(EstadoProveedorTableSeeder::class);

        $this->call(MutualTableSeeder::class);
        $this->call(InstitucionPensionTableSeeder::class);
        $this->call(InstitucionPrevisionTableSeeder::class);
        $this->call(CajaCompensacionTableSeeder::class);
        $this->call(BancoTableSeeder::class);

        $this->call(PeriodoTableSeeder::class);
        $this->call(CuentaTableSeeder::class);
        $this->call(PersonaTableSeeder::class);
        $this->call(ProveedorTableSeeder::class);
        $this->call(ClienteTableSeeder::class);
        $this->call(CentroCostoTableSeeder::class);
        // $this->call(CotizacionTableSeeder::class);
        $this->call(CuentaBancariaTableSeeder::class);
        // $this->call(GastoTableSeeder::class);
        $this->call(AuxiliarVisualTableSeeder::class);
        $this->call(AsistenciaTableSeeder::class);
        $this->call(HaberDescuentoTableSeeder::class);

        $this->call(MenuTableSeeder::class);
        $this->call(NotificacionTableSeeder::class);

        Artisan::call('passport:install');

        Artisan::call('scout:import', ['model' => 'App\CentroCosto']);
        Artisan::call('scout:import', ['model' => 'App\Cotizacion']);
        Artisan::call('scout:import', ['model' => 'App\Cuenta']);
        Artisan::call('scout:import', ['model' => 'App\Gasto']);
        Artisan::call('scout:import', ['model' => 'App\Cliente']);
        Artisan::call('scout:import', ['model' => 'App\Proveedor']);
        Artisan::call('scout:import', ['model' => 'App\Persona']);

        echo "\n" . DB::table('oauth_clients')->where('id', 2)->first()->secret . "\n";
        echo "Terminado en " . (microtime(1) - $inicio) . " segundos.\n";
    }
}
