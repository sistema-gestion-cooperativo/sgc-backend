<?php

use App\EstadoCotizacion;
use Illuminate\Database\Seeder;

class EstadoCotizacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $json = File::get('database/data/estados_cotizacion.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoCotizacion::create((array)$obj);
        }
        DB::statement("SELECT setval('estados_cotizacion_id_seq', COALESCE((SELECT MAX(id)+1 FROM estados_cotizacion), 1), false);");
    }
}