<?php

use App\Banco;
use Illuminate\Database\Seeder;

class BancoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/bancos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Banco::create((array)$obj);
        }
        DB::statement("SELECT setval('bancos_id_seq', COALESCE((SELECT MAX(id)+1 FROM bancos), 1), false);");
    }
}