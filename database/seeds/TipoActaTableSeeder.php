<?php

use App\TipoActa;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoActaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tipos_acta.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            TipoActa::create((array) $obj);
        }
    }
}