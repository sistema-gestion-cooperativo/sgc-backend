<?php

use App\Cotizacion;
use Illuminate\Database\Seeder;

class CotizacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/cotizaciones.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Cotizacion::create((array)$obj);
        }
        DB::statement("SELECT setval('cotizaciones_id_seq', COALESCE((SELECT MAX(id)+1 FROM cotizaciones), 1), false);");
    }
}