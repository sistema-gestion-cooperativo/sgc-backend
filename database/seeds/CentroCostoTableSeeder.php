<?php

use App\CentroCosto;
use Illuminate\Database\Seeder;

class CentroCostoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/centro_costos.json');
        $data = json_decode($json, true);
        foreach ($data as $obj) {
            try {
                CentroCosto::create($obj);
            } catch(Exception $e) {
                dd($obj, $e);
            }
        }
        DB::statement("SELECT setval('centro_costos_id_seq', COALESCE((SELECT MAX(id)+1 FROM centro_costos), 1), false);");
    }
}