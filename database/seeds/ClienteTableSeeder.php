<?php

use App\Cliente;
use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/clientes.json');
        $data = json_decode($json, true);
        foreach ($data as $obj) {
            try {
                Cliente::create($obj);
            } catch(Exception $e) {
                dd($obj, $e);
            }
        }
        DB::statement("SELECT setval('clientes_id_seq', COALESCE((SELECT MAX(id)+1 FROM clientes), 1), false);");
    }
}