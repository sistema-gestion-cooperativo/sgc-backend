<?php

use App\EstadoProveedor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/estados_proveedor.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            EstadoProveedor::create((array) $obj);
        }
    }
}