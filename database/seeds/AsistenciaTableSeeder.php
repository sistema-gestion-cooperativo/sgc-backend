<?php

use App\Asistencia;
use Illuminate\Database\Seeder;

class AsistenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/asistencias.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Asistencia::create((array)$obj);
        }
        DB::statement("SELECT setval('asistencias_id_seq', COALESCE((SELECT MAX(id)+1 FROM asistencias), 1), false);");
    }
}