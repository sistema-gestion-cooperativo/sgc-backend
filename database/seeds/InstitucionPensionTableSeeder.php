<?php

use App\InstitucionPension;
use Illuminate\Database\Seeder;

class InstitucionPensionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/instituciones_pensiones.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            InstitucionPension::create((array)$obj);
        }
        DB::statement("SELECT setval('instituciones_pensiones_id_seq', COALESCE((SELECT MAX(id)+1 FROM instituciones_pensiones), 1), false);");
    }
}