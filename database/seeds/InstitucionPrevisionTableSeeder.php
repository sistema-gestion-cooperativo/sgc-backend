<?php

use App\InstitucionPrevision;
use Illuminate\Database\Seeder;

class InstitucionPrevisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/instituciones_previsionales.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            InstitucionPrevision::create((array)$obj);
        }
        DB::statement("SELECT setval('instituciones_previsionales_id_seq', COALESCE((SELECT MAX(id)+1 FROM instituciones_previsionales), 1), false);");
    }
}