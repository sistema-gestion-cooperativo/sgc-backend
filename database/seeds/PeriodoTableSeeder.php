<?php

use App\Periodo;
use Illuminate\Database\Seeder;

class PeriodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/periodos.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Periodo::create((array)$obj);
        }
        DB::statement("SELECT setval('periodos_id_seq', COALESCE((SELECT MAX(id)+1 FROM periodos), 1), false);");
    }
}