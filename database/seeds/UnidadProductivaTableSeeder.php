<?php

use App\UnidadProductiva;
use Illuminate\Database\Seeder;

class UnidadProductivaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/unidades_productivas.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            UnidadProductiva::create((array)$obj);
        }
        DB::statement("SELECT setval('unidades_productivas_id_seq', COALESCE((SELECT MAX(id)+1 FROM unidades_productivas), 1), false);");
    }
}