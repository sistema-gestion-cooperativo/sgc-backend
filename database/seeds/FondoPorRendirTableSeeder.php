<?php

use App\FondoPorRendir;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FondoPorRendirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/fondos_por_rendir.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            FondoPorRendir::create((array) $obj);
        }
    }
}