#!/usr/bin/env bash
#Reset
php artisan migrate:reset
php artisan scaffold:create --json --single --model=Pais --plural=Paises --schema="nombre:string,iso2:string,iso3:string,prefijo_telefonico:string"

php artisan scaffold:create --json --model=DocumentoTributario --table=documentos_tributarios --schema="codigo:string,numero:integer,cliente_id:integer:foreign:nullable,proveedor_id:integer:foreign:nullable,nombre:string,cantidad:integer,unidad:string,precio:decimal(19,4),descripcion:text,presupuesto_materiales:decimal(19,4),presupuesto_mano_de_obra:decimal(19,4),presupuesto_excedentes:decimal(19,4),presupuesto_hospedaje:decimal(19,4),presupuesto_viatico:decimal(19,4),presupuesto_traslado:decimal(19,4),presupuesto_otros:decimal(19,4),mano_de_obra:json"
php artisan scaffold:create --json --model=TipoDocumentoTributario --table=tipo_documento_tributario --schema="codigo:string,numero:integer,cliente_id:integer:foreign:nullable,proveedor_id:integer:foreign:nullable,nombre:string,cantidad:integer,unidad:string,precio:decimal(19,4),descripcion:text,presupuesto_materiales:decimal(19,4),presupuesto_mano_de_obra:decimal(19,4),presupuesto_excedentes:decimal(19,4),presupuesto_hospedaje:decimal(19,4),presupuesto_viatico:decimal(19,4),presupuesto_traslado:decimal(19,4),presupuesto_otros:decimal(19,4),mano_de_obra:json"

29: Factura de Inicio
30: Factura
32: Factura de ventas y servicios no afectos o exentos de IVA
33: Factura Electrónica
34: Factura exenta Electrónica
40: Liquidación factura (ocultar)
43: Liquidación factura Electrónica
45: Factura de Compra (ocultar)
46: Factura de Compra Electrónica.
52: Guía de Despacho Electrónica
55: Nota de débito
56: Nota de débito Electrónica
60: Nota de crédito
61: Nota de crédito Electrónica
110: Factura de exportación.
111: Nota de Débito de Exportación.
112: Nota de Crédito de Exportación

dte: http://www.sii.cl/factura_electronica/formato_dte.pdf
codigos: http://www.sii.cl/factura_electronica/formato_iecv.pdf

php artisan scaffold:create --json --model=TipoGasto --table=tipos_gasto --schema="codigo:string,nombre:string"
php artisan scaffold:create --json --single --model=TipoProyecto --plural=TiposProyecto --schema="nombre:string"
php artisan scaffold:create --json --single --model=TipoCliente --plural=TiposCliente --schema="nombre:string"
php artisan scaffold:create --json --single --model=TipoDocumento --plural=TiposDocumento --schema="nombre:string"
php artisan scaffold:create --json --single --model=TipoDocumentoPago --plural=TiposDocumentoPago --schema="nombre:string"

php artisan scaffold:create --json --model=Mutual --plural=Mutuales --schema="codigo:string:nullable,nombre:string,nombre_presentacion:string:nullable"
php artisan scaffold:create --json --model=InstitucionPension --plural=InstitucionesPensiones --schema="codigo:string:nullable,nombre:string,nombre_presentacion:string:nullable"
php artisan scaffold:create --json --model=InstitucionPrevision --plural=InstitucionesPrevisionales --schema="codigo:string:nullable,nombre:string,nombre_presentacion:string:nullable"
php artisan scaffold:create --json --model=CajaCompensacion --plural=CajasCompensacion --schema="codigo:string:nullable,nombre:string,nombre_presentacion:string:nullable"
php artisan scaffold:create --json --model=Banco --plural=Bancos --schema="codigo:string:nullable,nombre:string,nombre_presentacion:string:nullable"

php artisan scaffold:create --json --model=UnidadProductiva --plural=UnidadProductivas --schema="rut:string,nombre:string,nombre_presentacion:string,logotipo:string:nullable,giro:string:nullable,webs:json:nullable,correos:string:nullable,telefonos:json:nullable,redes_sociales:json:nullable,comentarios:text:nullable,representante_legal_nombres:string,representante_legal_primer_apellido:string,representante_legal_segundo_apellido:string,representante_legal_rut:string,representante_legal_id:integer:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Periodo --plural=Periodos --schema="unidad_productiva_id:integer,mes:tinyinteger,ano:smallinteger,nombre:string:nullable,fecha_inicio:date:nullable,fecha_termino:date:nullable,abierto:boolean:default(false),deleted_at:softDeletes" #empresa,mes,año ->unique
php artisan scaffold:create --json --model=Cuenta --plural=Cuentas --schema="unidad_productiva_id:integer:foreign,nombre_plan:string:unique,codigo:string,cuenta_mayor:string,subcuenta:string,registro:string"
php artisan scaffold:create --json --model=Persona --plural=Personas --schema="unidad_productiva_id:integer:foreign,rut:string,nombres:string,primer_apellido:string,segundo_apellido:string:nullable,nombre_presentacion:string,correos:string:nullable,telefonos:json:nullable,direcciones:string:nullable,fecha_nacimiento:date:nullable,estado_civil:string:nullable,nacionalidad:string:nullable,foto_perfil:string:nullable,estado_persona:string:nullable,comentarios:text:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Rol --plural=Roles --schema="nombre:string,menu_defecto:json:nullable,comentarios:text:nullable"
php artisan scaffold:create --json --model=Permiso --plural=Permisos --schema="nombre:string,etiqueta:string,comentarios:text:nullable"
php artisan scaffold:create --json --model=Usuario --plural=Usuarios --schema="rut:string:unique,nombre_usuario:string,nombre_presentacion:string,avatar:string:nullable,contrasena:string:nullable,rol_id:integer:foreign,comentarios:text:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Proveedor --plural=Proveedores --schema="unidad_productiva_id:integer:foreign,rut:string,nombre:string,nombre_presentacion:string,direcciones:string:nullable,giro:string:nullable,webs:string:nullable,correos:string:nullable,telefonos:json:nullable,redes_sociales:json,datos_adicionales:json,calificaciones:json,comentarios:text:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Cliente --plural=Clientes --schema="unidad_productiva_id:integer:foreign,rut:string,nombre:string,nombre_presentacion:string,giro:string:nullable,webs:string:nullable,correos:string:nullable,telefonos:json:nullable,redes_sociales:json,datos_adicionales:json:nullable,calificaciones:json:nullable,comentarios:text:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Contacto --plural=Contactos --schema="unidad_productiva_id:integer:foreign,cliente_id:integer:foreign,nombre:string,correos:string:nullable,telefonos:json:nullable,cargo:string:nullable,comentarios:text:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Cotizacion --plural=Cotizaciones --schema="unidad_productiva_id:integer:foreign,codigo:string,nombre:string,cliente_id:integer:foreign,fecha_solicitud:date:nullable,fecha_envio:date:nullable,validez:integer:nullable,tipo_proyecto:string:nullable,estado_cotizacion:string,tipo_documento_id:integer:foreign,neto:decimal(19, 4):nullable,iva:decimal(19, 4):nullable,total:decimal(19, 4):nullable,responsable_id:integer:nullable,codigo_modificacion:string:nullable,cotizacion_id:integer,comentarios:text:nullable"
php artisan scaffold:create --json --model=CentroCosto --plural=CentroCostos --schema="unidad_productiva_id:integer:foreign,codigo:string,nombre:string,estado_centro_costo:string,adicional_de_id:integer:nullable,responsable_id:integer:nullable,deleted_at:softDeletes"
php artisan scaffold:create --json --model=Gasto --plural=Gastos --schema="unidad_productiva_id:integer:foreign,codigo:string,folio:string,fecha:date:nullable,periodo_mes:smallinteger,periodo_ano:smallinteger,monto:decimal(19, 4),neto:decimal(19, 4),iva:decimal(19, 4),bruto:decimal(19, 4),centro_costos_codigo:string,cuenta_id:integer:foreign,cuenta_bancaria_id:integer:foreign,deleted_at:softDeletes"

php artisan scaffold:create --json --model=CuentaBancaria --plural=CuentasBancarias --schema="unidad_productiva_id:integer:foreign,banco_id:integer:foreign,nombre:string"

php artisan scaffold:create --json --model=Menu --plural=Menus --schema="usuario_id:integer:foreign,menu:json,deleted_at:softDeletes"
php artisan scaffold:create --json --model=AuxiliarVisual --plural=AuxiliaresVisuales --schema="concepto:string,objeto:json"


php artisan scaffold:create --json --model=Asistencia --plural=Asistencias --schema="unidad_productiva_id:integer:foreign,persona_id:integer:foreign,hora_legal_entrada:datetime,hora_legal_salida:datetime,hora_extra_entrada:datetime,hora_extra_salida:datetime,hora_legal_total:integer:default(0),hora_extra_total:integer:default(0),fecha:date:nullable,centro_costo_id:integer"
php artisan scaffold:create --json --model=Contrato --plural=Contratos --schema="unidad_productiva_id:integer:foreign,persona_id:integer:foreign,contrato_id:integer:foreign,numero:integer:nullable,sueldo_base:decimal(19, 4),fecha_firma:date:nullable,fecha_vigencia:date:nullable,fecha_vencimiento:date"
php artisan scaffold:create --json --model=Oportunidad --plural=Oportunidades --schema="unidad_productiva_id:integer:foreign,fecha_vencimiento:date:nullable,probabilidad:integer,monto:decimal(19, 4),comentarios:text:nullable,prioridad:integer:nullable,persona_id:integer:foreign"

#Periodos
#UnidadProductivas
php artisan make:migration:schema add_mutual_id_to_empresas_table --schema="mutual_id:integer:foreign"
php artisan make:migration:schema add_caja_compensacion_id_to_empresas_table --schema="caja_compensacion_id:integer:foreign"
php artisan make:migration:pivot empresas actividades_economicas

#personas
#usuarios
php artisan make:migration:pivot usuarios roles
php artisan make:migration:pivot empresas roles
php artisan make:migration:pivot roles permisos

#centro de costos
php artisan make:migration:pivot presupuestos centro_costos

#clientes
php artisan make:migration:pivot clientes contactos

#cotizaciones

#proyectos

#proveedores

#gastos

#remuneraciones
php artisan make:migration:schema create_remuneraciones_table --model=Remuneracion --create=remuneraciones
php artisan make:migration:schema create_haberes_descuentos_table --model=HaberDescuento --create=haberes_descuentos
php artisan make:migration:schema create_contratos_table --model=Contrato --schema=""

#Datos y Tipos Fijos
php artisan make:migration:schema create_actividades_economicas_table --model=ActividadEconomica --schema=""
php artisan make:controller --resource --model=ActividadEconomica ActividadEconomicaController
