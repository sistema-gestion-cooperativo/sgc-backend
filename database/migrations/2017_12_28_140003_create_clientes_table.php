<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('estado_cliente_id')->nullable();
            $table->foreign('estado_cliente_id')->references('id')->on('estados_cliente');
            $table->string('rut')->nullable();
            $table->string('nombre');
            $table->string('nombre_presentacion');
            $table->string('giro')->nullable();
            $table->string('web')->nullable();
            $table->string('correo')->nullable();
            $table->string('telefono')->nullable();
            $table->json('contactos')->nullable();
            $table->json('redes_sociales')->nullable();
            $table->json('datos_adicionales')->nullable();
            $table->json('calificaciones')->nullable();
            $table->decimal('descuento', 4, 2)->nullable();
            $table->text('comentarios')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
