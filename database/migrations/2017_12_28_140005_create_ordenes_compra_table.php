<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenesCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes_compra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('centro_costo_id')->nullable();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->string('codigo');
            $table->string('nombre');
            $table->integer('cliente_id')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes'); //Si está presente, es una orden enviada
            $table->integer('proveedor_id')->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores'); //Si está presente, es una orden recibida
            $table->date('fecha_solicitud')->nullable();
            $table->date('fecha_envio')->nullable();
            $table->integer('validez')->nullable();
            $table->integer('tipo_cotizacion_id')->nullable();
            $table->foreign('tipo_cotizacion_id')->references('id')->on('tipos_cotizacion');
            $table->integer('estado_cotizacion_id')->nullable();
            $table->foreign('estado_cotizacion_id')->references('id')->on('estados_cotizacion');
            //$table->string('estados_cotizacion')->default('En espera');
            $table->integer('tipo_documento_id')->nullable();
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documento');
            $table->integer('monto_exento')->nullable();
            $table->integer('monto_afecto')->nullable();
            $table->integer('neto')->nullable();
            $table->integer('iva')->nullable();
            $table->integer('total')->nullable();
            $table->integer('responsable_id')->nullable();
            $table->foreign('responsable_id')->references('id')->on('personas');
            $table->string('codigo_modificacion')->nullable();
            $table->integer('cotizacion_id')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cotizaciones');
    }
}
