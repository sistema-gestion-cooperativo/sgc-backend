<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->integer('centro_costo_id')->unsigned();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('tipo_hora_id')->unsigned();
            $table->foreign('tipo_hora_id')->references('id')->on('tipos_hora');
            $table->tinyinteger('hora_entrada')->unsigned();
            $table->tinyinteger('minuto_entrada')->unsigned();
            $table->tinyinteger('hora_salida')->unsigned()->nullable();
            $table->tinyinteger('minuto_salida')->unsigned()->nullable();
            $table->decimal('total', 4, 2)->default(0);
            $table->date('fecha');
            $table->timestamps();
            $table->index(['persona_id', 'fecha']);
            $table->unique(['persona_id','hora_entrada', 'fecha']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencias');
    }
}
