<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('estado_cuenta_id')->nullable();
            $table->foreign('estado_cuenta_id')->references('id')->on('estados_cuenta');
            $table->string('nombre_plan');
            $table->string('codigo');
            $table->string('cuenta_mayor')->nullable();
            $table->string('subcuenta')->nullable();
            $table->string('registro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuentas');
    }
}
