<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonasTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personas', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('unidad_productiva_id')->unsigned();
			$table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');

			//Usuario
			$table->string('nombre_usuario')->nullable()	->unique();
			$table->string('contrasena')->nullable();
			$table->integer('rol_id')->unsigned();
			$table->foreign('rol_id')->references('id')->on('roles');
			$table->string('remember_token', 100)->nullable();

			$table->string('rut');
			$table->string('nombres');
			$table->string('primer_apellido');
			$table->string('segundo_apellido')->nullable();
			$table->string('nombre_presentacion');
			$table->string('correos')->nullable();
			$table->json('telefonos')->nullable();
			$table->json('contactos_emergencia')->nullable();
			$table->string('direcciones')->nullable();
			$table->string('nacionalidad')->nullable();
			$table->date('fecha_nacimiento')->nullable();
			$table->date('fecha_asociacion')->nullable();
			$table->string('estado_civil')->nullable();
			$table->integer('estado_persona_id')->nullable();
			$table->foreign('estado_persona_id')->references('id')->on('estados_persona');
			$table->string('foto_perfil')->nullable();
			$table->text('comentarios')->nullable();

			$table->integer('contrato_vigente_id')->unsigned()->nullable();
            // $table->foreign('contrato_vigente_id')->references('id')->on('contratos');

			$table->integer('institucion_prevision_id')->nullable();
			$table->foreign('institucion_prevision_id')->references('id')->on('instituciones_previsionales');
			$table->integer('institucion_pension_id')->nullable();
			$table->foreign('institucion_pension_id')->references('id')->on('instituciones_pensiones');
			$table->decimal('uf_prevision', 4, 2)->nullable();

			$table->double('vacaciones_disponibles', 4, 2)->default(0);

			$table->softDeletes('deleted_at');
			$table->timestamps();

			$table->unique(['unidad_productiva_id', 'rut']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personas');
	}
}
