<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarioChileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_chile', function (Blueprint $table) {
            $table->date('fecha')->primary();
            $table->integer('ano')->unsigned();
            $table->tinyinteger('numero_mes')->unsigned();
            $table->tinyinteger('numero_dia')->unsigned();
            // $table->string('estacion');
            $table->string('nombre_dia');
            $table->string('nombre_mes');
            $table->tinyinteger('dia_semana')->unsigned();
            $table->smallinteger('dia_ano')->unsigned();
            $table->tinyinteger('semana_ano')->unsigned();
            // $table->tinyinteger('semana_mes')->unsigned();
            $table->date('primer_dia_semana');
            $table->date('ultimo_dia_semana');
            // $table->tinyinteger('dia_semana_mes')->unsigned();
            // $table->tinyinteger('dia_semana_ano')->unsigned();
            $table->boolean('es_feriado')->default(false);
            $table->boolean('es_habil')->default(true);
            $table->boolean('es_dia_semana');
            $table->string('nombre_feriado')->nullable();
            $table->string('tipo_feriado')->nullable();
            $table->tinyinteger('feriados_mes')->unsigned();
            $table->tinyinteger('habiles_mes')->unsigned();
            $table->date('previo_habil');
            $table->date('proximo_habil');
            $table->tinyinteger('dias_mes')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendario_chile');
    }
}
