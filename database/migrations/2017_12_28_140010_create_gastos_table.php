<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGastosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->string('codigo')->nullable();
            $table->string('folio')->nullable();
            $table->integer('proveedor_id')->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->date('fecha_documento')->nullable();
            $table->date('fecha_pago')->nullable();
            $table->smallinteger('periodo_mes')->unsigned();
            $table->smallinteger('periodo_ano')->unsigned();
            $table->smallinteger('iva_mes')->unsigned();
            $table->smallinteger('iva_ano')->unsigned();
            $table->string('cheque_numero')->nullable();
            $table->string('fpr_numero')->nullable();
            $table->integer('monto_afecto')->default(0);
            $table->integer('monto_exento')->default(0);
            $table->integer('monto_otros_impuestos')->default(0);
            $table->integer('monto_neto')->nullable();
            $table->integer('monto_iva')->nullable();
            $table->integer('monto_total')->nullable();
            $table->integer('tipo_documento_id')->nullable();
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documento');
            $table->integer('tipo_documento_pago_id')->nullable();
            $table->foreign('tipo_documento_pago_id')->references('id')->on('tipos_documento_pago');
            $table->integer('centro_costo_id')->nullable();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('cuenta_id')->nullable();
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->integer('tipo_gasto_id')->nullable();
            $table->foreign('tipo_gasto_id')->references('id')->on('tipos_gasto');
            $table->integer('cuenta_codigo')->nullable();
            $table->integer('cuenta_bancaria_id')->nullable();
            $table->text('comentarios')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gastos');
    }
}
