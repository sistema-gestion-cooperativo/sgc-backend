<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->unsigned();
            $table->integer('unidad_productiva_id')->nullable();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('centro_costo_id')->unsigned()->nullable();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('responsable_id')->unsigned();
            $table->foreign('responsable_id')->references('id')->on('personas');
            $table->integer('proveedor_id')->unsigned()->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->integer('monto')->nullable();
            $table->date('fecha_cheque')->nullable();
            $table->date('fecha_endoso')->nullable();
            $table->date('fecha_cobro')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cheques');
    }
}
