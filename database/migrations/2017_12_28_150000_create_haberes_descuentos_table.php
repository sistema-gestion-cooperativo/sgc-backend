<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHaberesDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haberes_descuentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->string('codigo')->nullable();
            $table->string('tipo');
            $table->integer('monto');
            $table->decimal('porcentaje', 3, 3)->nullable(); //Debe exisitir monto o porcentaje. Si existen ambos el monto se considera un multiplicador
            $table->string('nombre_presentacion');
            $table->string('descripcion')->nullable();
            $table->boolean('imponible')->default(true);
            $table->boolean('tributable')->default(false);
            $table->boolean('hora_extra')->default(false);
            $table->boolean('gratificacion')->default(false);
            $table->boolean('semana_corrida')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('haberes_descuentos');
    }
}
