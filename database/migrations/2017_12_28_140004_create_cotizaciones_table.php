<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned()->index();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('centro_costo_id')->nullable()->index();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('codigo')->unsigned()->index()->index();
            $table->string('nombre_presentacion')->default('Sin nombre');
            $table->integer('cliente_id')->nullable()->index();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->date('fecha_solicitud')->nullable();
            $table->date('fecha_envio')->nullable();
            $table->integer('validez')->nullable();
            $table->integer('tipo_cotizacion_id')->nullable();
            $table->foreign('tipo_cotizacion_id')->references('id')->on('tipos_cotizacion');
            $table->integer('estado_cotizacion_id')->nullable()->index();
            $table->foreign('estado_cotizacion_id')->references('id')->on('estados_cotizacion');
            $table->integer('tipo_documento_id')->nullable();
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documento');
            $table->integer('valor_cip')->nullable();
            $table->integer('valor_uf')->nullable();
            $table->integer('valor_hora')->nullable();
            $table->integer('porcentaje_utilidad')->nullable();
            $table->integer('porcentaje_descuento')->nullable();
            $table->integer('monto_utilidad')->nullable();
            $table->integer('monto_descuento')->nullable();
            $table->integer('monto_exento')->nullable();
            $table->integer('monto_afecto')->nullable();
            $table->integer('monto_neto')->nullable();
            $table->integer('monto_iva')->nullable();
            $table->integer('monto_total')->nullable()->index();
            $table->integer('responsable_cotizacion_id')->nullable();
            $table->foreign('responsable_cotizacion_id')->references('id')->on('personas');
            $table->integer('responsable_proyecto_id')->nullable();
            $table->foreign('responsable_proyecto_id')->references('id')->on('personas');
            $table->string('codigo_modificacion')->nullable();
            $table->integer('cotizacion_id')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();

            $table->unique(['unidad_productiva_id', 'codigo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cotizaciones');
    }
}
