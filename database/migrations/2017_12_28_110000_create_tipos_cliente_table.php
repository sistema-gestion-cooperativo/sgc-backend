<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_presentacion');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipos_cliente');
    }
}
