<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFondosPorRendirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fondos_por_rendir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->unsigned();
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('centro_costo_id')->unsigned()->nullable();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('responsable_id')->unsigned();
            $table->foreign('responsable_id')->references('id')->on('personas');
            $table->integer('monto');
            $table->integer('diferencia')->nullable();
            $table->datetime('fecha_entrega');
            $table->datetime('fecha_cierre')->nullable();
            $table->datetime('fecha_devolucion')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fondos_por_rendir');
    }
}
