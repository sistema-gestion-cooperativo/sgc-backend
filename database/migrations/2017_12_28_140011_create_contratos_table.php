<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->integer('contrato_id')->unsigned();
            $table->foreign('contrato_id')->references('id')->on('contratos');
            $table->integer('numero')->nullable();
            $table->integer('horas_semana')->nullable();
            $table->integer('sueldo_base');
            $table->integer('gratificacion');
            $table->json('bonos')->nullable();
            $table->integer('centro_costo_id')->nullable();
            $table->foreign('centro_costo_id')->references('id')->on('centro_costos');
            $table->integer('plazo')->nullable();
            $table->string('cargo')->nullable();
            $table->string('lugar_de_trabajo')->nullable();
            $table->string('jornada')->default(false);
            $table->boolean('a_plazo')->default(true);
            $table->boolean('bono_semana_corrida')->default(false);
            // $table->boolean('gratificacion')->default(false);
            $table->date('fecha_firma')->nullable();
            $table->date('fecha_vigencia')->nullable();
            $table->date('fecha_termino')->nullable();
            $table->date('fecha_vencimiento');
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}
