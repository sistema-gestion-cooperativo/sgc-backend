<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('tipo_acta_id')->unsigned();
            $table->foreign('tipo_acta_id')->references('id')->on('tipos_acta');
            $table->integer('numero')->unsigned();
            $table->datetime('fecha');
            $table->time('hora_inicio');
            $table->time('hora_termino');
            $table->text('observaciones');
            $table->integer('actuario_id')->unsigned();
            $table->foreign('actuario_id')->references('id')->on('personas');
            $table->integer('moderador_id')->unsigned();
            $table->foreign('moderador_id')->references('id')->on('personas');
            $table->json('temas'); //estructura: numero, titulo, contenido
            $table->json('acuerdos'); //estructura: numero, titulo, contenido
            $table->json('compromisos'); //estructura: numero, titulo, contenido, responsable, fecha_termino
            $table->json('temas_proxima_asamblea'); //estructura: prioridad, titulo, contenido
            $table->string('titulos_tema')->nullable();
            $table->string('titulos_acuerdo')->nullable();
            $table->string('titulos_compromiso')->nullable();
            $table->string('palabras_clave')->nullable();
            $table->text('comentarios')->nullable();

            $table->timestamps();
        }
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actas');
    }
}
