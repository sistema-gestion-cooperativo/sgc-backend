<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnidadesProductivasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades_productivas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut');
            $table->string('nombre');
            $table->string('nombre_presentacion');
            $table->string('logotipo')->nullable();
            $table->string('giro')->nullable();
            $table->string('web')->nullable();
            $table->string('direcciones')->nullable();
            $table->string('correos')->nullable();
            $table->string('telefonos')->nullable();
            $table->json('redes_sociales')->nullable();
            $table->text('comentarios')->nullable();
            $table->string('representante_legal_nombres');
            $table->string('representante_legal_primer_apellido');
            $table->string('representante_legal_segundo_apellido');
            $table->string('representante_legal_rut');
            $table->integer('representante_legal_id')->nullable();
            $table->integer('valor_cip')->nullable();

            $table->double('factor_mutual', 4, 2)->nullable();

            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unidades_productivas');
    }
}
