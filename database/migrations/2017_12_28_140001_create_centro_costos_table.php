<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCentroCostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_costos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->string('codigo');
            $table->string('nombre_presentacion');
            $table->integer('ultima_cotizacion_id')->nullable();
            // $table->foreign('ultima_cotizacion_id')->references('id')->on('cotizaciones'); // TODO: Crear fk cuando este up
            $table->integer('estado_centro_costo_id')->nullable();
            $table->foreign('estado_centro_costo_id')->references('id')->on('estados_centro_costo');
            $table->integer('adicional_de_centro_costo_codigo')->nullable()->unsigned();
            $table->integer('adicional_de_centro_costo_id')->nullable();
            // $table->foreign('adicional_de_centro_costo_id')->references('id')->on('centro_costos'); //TODO: Relaciona id la bd antigua de luis lo que no da integridad 
            $table->integer('responsable_id')->nullable();
            $table->foreign('responsable_id')->references('id')->on('personas');
            $table->integer('monto')->nullable(); //TODO: Solo monto o monto excento-afecto?
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_cierre')->nullable();
            $table->text('comentarios')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
			$table->unique(['unidad_productiva_id', 'codigo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centro_costos');
    }
}
