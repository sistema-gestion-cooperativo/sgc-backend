<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBalancesObraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balances_obra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned()->index();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('periodo_id')->unsigned()->index();
            $table->foreign('periodo_id')->references('id')->on('periodos');
            $table->integer('centro_costos_id')->unsigned()->index();
            $table->foreign('centro_costos_id')->references('id')->on('centro_costos');
            $table->integer('cliente_id')->unsigned()->index();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->integer('responsable_proyecto_id')->unsigned();
            $table->foreign('responsable_proyecto_id')->references('id')->on('personas');
            $table->integer('responsable_cotizacion_id')->unsigned();
            $table->foreign('responsable_cotizacion_id')->references('id')->on('personas');

      //Lookup table
            $table->string('cotizacion_codigo')->index();
            $table->string('centro_costos_codigo');
            $table->string('centro_costos_nombre');
            $table->string('responsable_ejecucion_nombre');
            $table->string('responsable_cotizacion_nombre');
            $table->integer('costo_hora_cip')->nullable();

            $table->string('codigo'); //Más entidades deben tener código: ¿Cómo tener un contador correlativo propio en un sistema multiempresa?
            $table->datetime('fecha');
            $table->integer('valor_proyecto');
            $table->integer('valor_adicionales')->nullable();
            $table->integer('valor_total');

            $table->integer('numero_gastos')->nullable();

            $table->integer('costo_real_materiales')->nullable();
            $table->integer('costo_real_mano_de_obra')->nullable();
            $table->integer('costo_real_excedentes')->nullable();
            $table->integer('costo_real_hospedaje')->nullable();
            $table->integer('costo_real_viatico')->nullable();
            $table->integer('costo_real_traslado')->nullable();
            $table->integer('costo_real_otros')->nullable();

            $table->integer('presupuesto_materiales')->nullable();
            $table->integer('presupuesto_mano_de_obra')->nullable();
            $table->integer('presupuesto_excedentes')->nullable();
            $table->integer('presupuesto_hospedaje')->nullable();
            $table->integer('presupuesto_viatico')->nullable();
            $table->integer('presupuesto_traslado')->nullable();
            $table->integer('presupuesto_otros')->nullable();

            $table->integer('diferencia_materiales')->nullable();
            $table->integer('diferencia_mano_de_obra')->nullable();
            $table->integer('diferencia_excedentes')->nullable();
            $table->integer('diferencia_hospedaje')->nullable();
            $table->integer('diferencia_viatico')->nullable();
            $table->integer('diferencia_traslado')->nullable();
            $table->integer('diferencia_otros')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('balances_obra');
    }
}
