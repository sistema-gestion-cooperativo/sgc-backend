<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_productiva_id')->unsigned();
            $table->foreign('unidad_productiva_id')->references('id')->on('unidades_productivas');
            $table->integer('estado_notificacion_id')->nullable();
            $table->foreign('estado_notificacion_id')->references('id')->on('estados_notificacion');            
            $table->integer('creador_id')->nullable();
            $table->foreign('creador_id')->references('id')->on('personas');
            $table->string('nombre_presentacion');
            $table->string('color');
            $table->string('icono')->nullable();
            $table->text('contenido')->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notificaciones');
    }
}
