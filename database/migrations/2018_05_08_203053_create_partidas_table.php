<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cotizacion_id')->nullable();
            $table->foreign('cotizacion_id')->references('id')->on('cotizaciones');
            $table->string('codigo')->nullable();
            $table->integer('numero')->unsigned();
            $table->string('nombre_presentacion');
            $table->integer('cantidad')->unsigned();
            $table->string('unidad');
            $table->integer('precio');
            $table->text('descripcion')->nullable();
            $table->integer('presupuesto_materiales')->nullable();
            $table->integer('presupuesto_mano_de_obra')->nullable();
            $table->integer('presupuesto_excedentes')->nullable();
            $table->integer('presupuesto_hospedaje')->nullable();
            $table->integer('presupuesto_viatico')->nullable();
            $table->integer('presupuesto_traslado')->nullable();
            $table->integer('presupuesto_otros')->nullable();
            $table->json('mano_de_obra')->nullable();
            //TODO: Visibilidad 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partidas');
    }
}
